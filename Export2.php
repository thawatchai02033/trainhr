<?php
// Include the main TCPDF library (search for installation path).
require_once __DIR__ . '/tools/tcpdf/tcpdf.php';
require_once(__DIR__ . '/fonts/Sarabun/thsarabun.php');
//require_once('thsarabun.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if (isset($request)) {
    if (isset($request->html_data) && isset($request->Project_code) && isset($request->author) && isset($request->subject) && isset($request->title)) {
// create new PDF document
//        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//    $Creator = '';
//    $Author = '';
//    $Title = '';
//    $Subject = '';
//    $Keywords = '';
        $uiqueFileName = uniqid($request->Project_code . '_file', true);

// set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($request->author);
        $pdf->SetTitle($request->title);
        $pdf->SetSubject($request->subject);
        $pdf->SetKeywords('');

        $pdf->setPrintHeader(false);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// Add a page
        $pdf->AddPage('P','A4');

        //        $fontname = $pdf->addTTFfont(__DIR__ . '/tools/tcpdf/Sarabun/THSarabun.ttf', '', '', 32);
//        define('sarabun', TCPDF_FONTS::addTTFfont(dirname(__FILE__).'/tools/tcpdf/Sarabun/THSarabun.ttf', 'TrueTypeUnicode'));
//        define('PROMPT_BOLD', TCPDF_FONTS::addTTFfont(dirname(__FILE__).'/fonts/Prompt-Bold.ttf', 'TrueTypeUnicode'));
// use the font
//        $pdf->SetFont(sarabun, '', 14, '', false);
        $pdf->SetFont('thsarabun', '', 14);

//$html = '<span style="text-align:justify;">a <u>abc</u> abcdefghijkl (abcdef) abcdefg <b>abcdefghi</b> a ((abc)) abcd <img src="images/logo_example.png" border="0" height="41" width="41" /> <img src="images/tcpdf_box.svg" alt="test alt attribute" width="80" height="60" border="0" /> abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg <b>abcdefghi</b> a <u>abc</u> abcd abcdef abcdefg <b>abcdefghi</b> a abc \(abcd\) abcdef abcdefg <b>abcdefghi</b> a abc \\\(abcd\\\) abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg <b>abcdefghi</b> a abc abcd abcdef abcdefg abcdefghi a abc abcd <a href="http://tcpdf.org">abcdef abcdefg</a> start a abc before <span style="background-color:yellow">yellow color</span> after a abc abcd abcdef abcdefg abcdefghi a abc abcd end abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi a abc abcd abcdef abcdefg abcdefghi<br />abcd abcdef abcdefg abcdefghi<br />abcd abcde abcdef</span>';
        $html = $request->html_data;

// output the HTML content
        $pdf->writeHTML($html, true, 0, true, true);


// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
//$pdf->Output('hello-tcpdf.pdf', 'I');
        $pdf->Output(__DIR__ . '/fileReport/' . $uiqueFileName . '.pdf', 'F');
        $pdf->Close();
        if (is_file(__DIR__ . '/fileReport/' . $uiqueFileName . '.pdf')) {
            echo json_encode(array('status' => true, 'path' => '/fileReport/' . $uiqueFileName . '.pdf'));
        } else {
            echo json_encode(array('status' => false, 'path' => ''));
        }
    } else {
        echo json_encode(array('status' => false, 'path' => ''));
    }
} else {
    echo json_encode(array('status' => false, 'path' => ''));
}
