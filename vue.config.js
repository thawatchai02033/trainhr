const path = require( 'path' );
const CKEditorWebpackPlugin = require( '@ckeditor/ckeditor5-dev-webpack-plugin' );
const { styles } = require( '@ckeditor/ckeditor5-dev-utils' );

module.exports = {
  devServer: {
    proxy: {
      '/MedHr': {
        target: 'https://medhr.medicine.psu.ac.th',
        changeOrigin: true,
        secure:false,
        pathRewrite: {'^/MedHr': ''},
        logLevel: 'debug'
      },
      '/MedNet': {
        target: 'https://mednet.psu.ac.th',
        changeOrigin: true,
        secure:false,
        pathRewrite: {'^/MedNet': ''},
        logLevel: 'debug'
      },
      '/Line': {
        target: 'https://notify-api.line.me',
        changeOrigin: true,
        secure:false,
        pathRewrite: {'^/Line': ''},
        logLevel: 'debug'
      }
    }
  },
  publicPath: './',
  "transpileDependencies": [
    "vuetify",
    /ckeditor5-[^/\\]+[/\\]src[/\\].+\.js$/,
  ],

  configureWebpack: {
    plugins: [
      // CKEditor needs its own plugin to be built using webpack.
      new CKEditorWebpackPlugin( {
        // See https://ckeditor.com/docs/ckeditor5/latest/features/ui-language.html
        language: 'th',
        /**
         * Sets the behavior of the <kbd>Enter</kbd> key. It also determines other behavior
         * rules of the editor, like whether the `<br>` element is to be used
         * as a paragraph separator when indenting text.
         * The allowed values are the following constants that cause the behavior outlined below:
         *
         * * {@link CKEDITOR#ENTER_P} (1) &ndash; New `<p>` paragraphs are created.
         * * {@link CKEDITOR#ENTER_BR} (2) &ndash; Lines are broken with `<br>` elements.
         * * {@link CKEDITOR#ENTER_DIV} (3) &ndash; New `<div>` blocks are created.
         *
         * **Note**: It is recommended to use the {@link CKEDITOR#ENTER_P} setting because of
         * its semantic value and correctness. The editor is optimized for this setting.
         *
         * Read more in the {@glink features/enterkey documentation} and see the
         * {@glink examples/enterkey example}.
         *
         *		// Not recommended.
         *		config.enterMode = CKEDITOR.ENTER_BR;
         *
         * @cfg {Number} [=CKEDITOR.ENTER_P]
         */
        enterMode: 2,
        shiftEnterMode: 3,
        // Append translations to the file matching the `app` name.
        translationsOutputFile: /app/,
      } )
    ]
  },

  // Vue CLI would normally use its own loader to load .svg and .css files, however:
  //	1. The icons used by CKEditor must be loaded using raw-loader,
  //	2. The CSS used by CKEditor must be transpiled using PostCSS to load properly.
  chainWebpack: config => {
    // (1.) To handle editor icons, get the default rule for *.svg files first:
    const svgRule = config.module.rule( 'svg' );

    // Then you can either:
    //
    // * clear all loaders for existing 'svg' rule:
    //
    //		svgRule.uses.clear();
    //
    // * or exclude ckeditor directory from node_modules:
    svgRule.exclude.add( path.join( __dirname, 'node_modules', '@ckeditor' ) );

    // Add an entry for *.svg files belonging to CKEditor. You can either:
    //
    // * modify the existing 'svg' rule:
    //
    //		svgRule.use( 'raw-loader' ).loader( 'raw-loader' );
    //
    // * or add a new one:
    config.module
        .rule( 'cke-svg' )
        .test( /ckeditor5-[^/\\]+[/\\]theme[/\\]icons[/\\][^/\\]+\.svg$/ )
        .use( 'raw-loader' )
        .loader( 'raw-loader' );

    // (2.) Transpile the .css files imported by the editor using PostCSS.
    // Make sure only the CSS belonging to ckeditor5-* packages is processed this way.
    config.module
        .rule( 'cke-css' )
        .test( /ckeditor5-[^/\\]+[/\\].+\.css$/ )
        .use( 'postcss-loader' )
        .loader( 'postcss-loader' )
        .tap( () => {
          return styles.getPostCssConfig( {
            themeImporter: {
              themePath: require.resolve( '@ckeditor/ckeditor5-theme-lark' ),
            },
            minify: true
          } );
        } );
  }
}
