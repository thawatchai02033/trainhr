import {ServiceProject} from '@/resource'

const AppointmentModule = {
    namespaced: true,
    state: {
        AppointmentData: [],
        AppointmentGroupData: [],
        AppointmentCommandData: [],
        FileData: [],
        FormPrint: [],
    },
    mutations: {
        SET_APPOINTMENT_DATA(state, data) {
            state.AppointmentData = []
            if (data.length > 0) {
                state.AppointmentData = data
            } else {
                if (data != null) {
                    state.AppointmentData.push(data)
                }
            }
        },
        SET_APPOINTMENTGROUP_DATA(state, data) {
            state.AppointmentGroupData = []
            if (data.length > 0) {
                state.AppointmentGroupData = data
            } else {
                if (data != null) {
                    state.AppointmentGroupData.push(data)
                }
            }
        },
        SET_FILE_DATA(state, data) {
            state.FileData = []
            if (data.length > 0) {
                state.FileData = data
            } else {
                if (data != null) {
                    state.FileData.push(data)
                }
            }
        },
        SET_APPOINTMENT_COMMAND_DATA(state, data) {
            state.AppointmentCommandData = []
            if (data.length > 0) {
                state.AppointmentCommandData = data
            } else {
                if (data != null) {
                    state.AppointmentCommandData.push(data)
                }
            }
        },
        SET_FORM_PRINT_DATA(state, data) {
            state.FormPrint = []
            if (data != null) {
                if (data.length > 0) {
                    state.FormPrint = data
                } else {
                    state.FormPrint.push(data)
                }
            }
        },
    },
    actions: {
        initAppointment({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Appointment_view/active/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_APPOINTMENT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initAppointmentGroup({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Appointment_Team_view/used/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_APPOINTMENTGROUP_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getAppointment({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Appointment_view/id/' + data.id + '/active/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_APPOINTMENT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getAppointmentGroup({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Appointment_Team_view/Ref_id/' + data.Ref_id + '/used/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_APPOINTMENTGROUP_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initFileData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/train_files/active/1/Ref_id/' + data.Ref_id + '/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_FILE_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initAppointmentCommandData({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Appointment_Command_Ms/').then((res) => {
                    if(res.data){
                        commit('SET_APPOINTMENT_COMMAND_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initFormPrintData({ commit }, data ) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Appointment_Form_Print/Appointment_id/' + data.id).then((res) => {
                    if(res.data){
                        commit('SET_FORM_PRINT_DATA', res.data)
                        resolve(true)
                    } else {
                        commit('SET_FORM_PRINT_DATA', null)
                        resolve(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_FORM_PRINT_DATA', null)
                        reject(true)
                    }
                })
            })
        },
    },
    getters: {
        getAppointmentData(state) {
            return state.AppointmentData
        },
        getAppointmentGroupData(state) {
            return state.AppointmentGroupData
        },
        getAppointmentCommandData(state) {
            return state.AppointmentCommandData
        },
        getFileData(state) {
            return state.FileData
        },
        getFormPrintData(state) {
            return state.FormPrint
        }
    }
}

export default AppointmentModule
