import {ServiceStaff} from '../resource'
// import {ServiceStaff, ServiceProvider} from '../resource'
import ConstData from "@/const/Const";
import axios from "axios";
const queryString = require('query-string');

const staffModule = {
    namespaced: true,
    state: {
        AdminData: [],
        MeData: [],
        StaffData: [],
        StaffAllData: [],
        PositionsData: [],
        CardData: [],
        CardData2: [],
        ApproveMaster: [],
        AllApproveMaster: [],
        AcademicFunds: [],
        AcademicFundsByPerson: [],
    },
    mutations: {
        SET_ME_DATA(state, data) {
            state.MeData = []
            if (data.length > 0) {
                state.MeData = data
            } else {
                state.MeData.push(data)
            }
        },
        SET_ADMIN_DATA(state, data) {
            state.AdminData = []
            if (data.length > 0) {
                state.AdminData = data
            } else {
                state.AdminData.push(data)
            }
        },
        SET_STAFF_DATA(state, data) {
            state.StaffData = []
            if (data.length > 0) {
                state.StaffData = data
            } else {
                state.StaffData.push(data)
            }
        },
        SET_STAFF_ALL_DATA(state, data) {
            state.StaffAllData = []
            if (data.length > 0) {
                state.StaffAllData = data
            } else {
                state.StaffAllData.push(data)
            }
        },
        SET_POSITIONS_DATA(state, data) {
            state.PositionsData = []
            if (data.length > 0) {
                state.PositionsData = data
            } else {
                state.PositionsData.push(data)
            }
        },
        SET_CARD_DATA(state, data) {
            state.CardData = []
            if (data.length > 0) {
                state.CardData = data
            } else {
                state.CardData.push(data)
            }
        },
        SET_APPROVE_MASTER(state, data) {
            state.ApproveMaster = []
            if (data.length > 0) {
                state.ApproveMaster = data
            } else {
                state.ApproveMaster.push(data)
            }
        },
        SET_APPROVE_MASTER_ALL(state, data) {
            state.AllApproveMaster = []
            if (data.length > 0) {
                state.AllApproveMaster = data
            } else {
                state.AllApproveMaster.push(data)
            }
        },
        SET_CARD_DATA2(state, data) {
            state.CardData2 = []
            if (data.length > 0) {
                state.CardData2 = data
            } else {
                state.CardData2.push(data)
            }
        },
        SET_ACADEMIC_FUNDS(state, data) {
            state.AcademicFunds = []
            if (data.length > 0) {
                state.AcademicFunds = data
            } else {
                state.AcademicFunds.push(data)
            }
        },
        SET_ACADEMIC_FUNDS_BY_PERSON(state, data) {
            state.AcademicFundsByPerson = []
            if (data.length > 0) {
                state.AcademicFundsByPerson = data
            } else {
                state.AcademicFundsByPerson.push(data)
            }
        },
        saveSessions(state, data) {
            axios.post(ConstData.ConnectionHost + '/saveSessions.php',  queryString.stringify({
                session: data.session
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            })
        },
        SendMail(data){
            axios.post(ConstData.ConnectionHost + '/sendMail/SendMailData.php',  queryString.stringify({
                perid_admin: data.peridToSend,
                perid: data.peridToReceive,
                headerName: data.headerName,
                header: data.header,
                body: data.body,
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            })
        },
        TestFunc(state,data) {
            console.log(state)
            console.log(data)
        }
    },
    actions: {
        initMeData({commit}) {
            return new Promise((resolve) => {
                if (getCookie('access_token') != '') {
                    // ServiceProvider.fetch('/staff/perid/' + parseJwt(getCookie('access_token')).perid).then(res => {
                   /* ServiceProvider.custom('POST', {
                        method: 'GET',
                        headers: '',
                        param: '',
                        query: 'staff/perid/' + parseJwt(getCookie('access_token')).perid,
                    }).then(res => {
                        if (res.data) {
                            commit('SET_ME_DATA', res.data)
                            resolve(true)
                        } else {
                            resolve(false)
                        }
                    }).catch(res => {
                        if (res){
                            reject(true)
                        }
                    })*/

                    axios.post(ConstData.ConnectionHost + '/HrTrain2/serviceAPI.php', {
                        queryName: 'staff',
                        param: {
                            perid: parseJwt(getCookie('access_token')).perid,
                        }
                    }).then(res => {
                        if (res.data) {
                            commit('SET_ME_DATA', res.data)
                            resolve(true)
                        } else {
                            resolve(false)
                        }
                    }).catch(res => {
                        if (res){
                            document.cookie = 'vurl=' + ConstData.ConnectionHost + '/HrTrain2'
                            window.location.href = ConstData.ConnectionHost + '/SingleSign/';
                            // reject(true)
                        }
                    })
                    // axios.get(ConstData.ConnectionHost + '/personnel/pm/profile/me/job_info').then(res => {
                    //     if (res.data.items) {
                    //         ServiceProvider.fetch('/staff/perid/' + parseJwt(getCookie('access_token')).perid).then(res => {
                    //             if (res.data) {
                    //                 commit('SET_ME_DATA', res.data)
                    //                 resolve(true)
                    //             } else {
                    //                 resolve(false)
                    //             }
                    //         })
                    //     } else {
                    //         resolve(false)
                    //     }
                    // })
                } else {
                    // document.cookie = 'access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwZXJpZCI6IjQ2NzkyIiwibmFtZSI6Ilx1MGUxOFx1MGUyN1x1MGUzMVx1MGUwYVx1MGUwYVx1MGUzMVx1MGUyMiIsInN1cm5hbWUiOiJcdTBlMDhcdTBlMzFcdTBlMTlcdTBlMTdcdTBlMjNcdTBlMTdcdTBlMzRcdTBlMWVcdTBlMjJcdTBlNGMifQ.YTW8R5wDrEPUPv5HkvfewbiQKBwpF18o5ZBxwZPtk1o'
                    // console.log(getCookie('access_token'))
                    // this.$cookies.remove('access_token')
                    // this.$session.set('vurl', ConstData.ConnectionHost + '/CardRegister/#/Card1')
                    // this.$cookies.set('vurl', ConstData.ConnectionHost + '/HrTrain2')
                    document.cookie = 'vurl=' + ConstData.ConnectionHost + '/HrTrain2'
                    window.location.href = ConstData.ConnectionHost + '/SingleSign/';
                    // window.location.href = ConstData.ConnectionHost + '/SingleSign/perid_login';
                }
            })
        },
        initMeData2({commit}, data) {
            return new Promise((resolve, reject) => {
                if (getCookie('access_token') != '') {
                    axios.get(ConstData.ConnectionHost + '/personnel/pm/profile/me/job_info').then(res => {
                        if (res.data.items) {
                            // ServiceProvider.fetch('/staff/perid/' + res.data.items.perid)
                            /*ServiceProvider.custom('POST', {
                                method: 'GET',
                                headers: '',
                                param: '',
                                query: 'staff/perid/' + res.data.items.perid,
                            }).then(res => {
                                if (res.data) {
                                    commit('SET_ME_DATA', res.data)
                                    resolve(true)
                                } else {
                                    resolve(false)
                                }
                            })*/
                            axios.post(ConstData.ConnectionHost + '/HrTrain2/serviceAPI.php', {
                                queryName: 'staff',
                                param: {
                                    perid: res.data.items.perid,
                                }
                            }).then(res => {
                                if (res.data) {
                                    commit('SET_ME_DATA', res.data)
                                    resolve(true)
                                } else {
                                    resolve(false)
                                }
                            }).catch(res => {
                                if (res){
                                    reject(true)
                                }
                            })
                        } else {
                            resolve(false)
                        }
                    }).catch(res => {
                        if (res) {
                            reject(true)
                        }
                    })
                } else {
                    // document.cookie = 'access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwZXJpZCI6IjQ2NzkyIiwibmFtZSI6Ilx1MGUxOFx1MGUyN1x1MGUzMVx1MGUwYVx1MGUwYVx1MGUzMVx1MGUyMiIsInN1cm5hbWUiOiJcdTBlMDhcdTBlMzFcdTBlMTlcdTBlMTdcdTBlMjNcdTBlMTdcdTBlMzRcdTBlMWVcdTBlMjJcdTBlNGMifQ.YTW8R5wDrEPUPv5HkvfewbiQKBwpF18o5ZBxwZPtk1o'
                    // console.log(getCookie('access_token'))
                    // this.$cookies.remove('access_token')
                    // this.$session.set('vurl', ConstData.ConnectionHost + '/CardRegister/#/Card1')
                    // this.$cookies.set('vurl', ConstData.ConnectionHost + '/HrTrain2')
                    document.cookie = 'vurl=' + ConstData.ConnectionHost + '/HrTrain2/' + data.path
                    window.location.href = ConstData.ConnectionHost + '/SingleSign/';
                    // window.location.href = ConstData.ConnectionHost + '/SingleSign/perid_login';
                }
            })
        },
        initAdminData({commit}) {
            return new Promise((resolve) => {
                ServiceStaff.initData('/hrtrain/D_project_admin/?limit=50000').then((res) => {
                    if (res.data) {
                        commit('SET_ADMIN_DATA', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                })
            })
        },
        initStaffData({commit}) {
            return new Promise((resolve) => {
                // ServiceStaff.initData('/staff/active/1/?limit=50000')
                /*ServiceProvider.custom('POST', {
                    method: 'GET',
                    headers: '',
                    param: '',
                    // query: 'staff/active/1/?limit=50000',
                    query: 'staff/?limit=50000',
                }).then((res) => {
                    if (res.data) {
                        commit('SET_STAFF_DATA', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                })*/

                axios.post(ConstData.ConnectionHost + '/HrTrain2/serviceAPI.php', {
                    queryName: 'staff',
                    param: {
                        perid: null
                    }
                }).then(res => {
                    if (res.data) {
                        commit('SET_STAFF_DATA', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res){
                        resolve(false)
                    }
                })
            })
        },
        initAllStaffData({commit}) {
            return new Promise((resolve) => {
                // ServiceStaff.initData('/staff/active/1/?limit=50000')
                /*ServiceProvider.custom('POST', {
                    method: 'GET',
                    headers: '',
                    param: '',
                    query: 'staff/?limit=50000',
                }).then((res) => {
                    if (res.data) {
                        commit('SET_STAFF_ALL_DATA', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                })*/

                axios.post(ConstData.ConnectionHost + '/HrTrain2/serviceAPI.php', {
                    queryName: 'staff',
                    param: {
                        perid: null,
                        cordi: 'all'
                    }
                }).then(res => {
                    if (res.data) {
                        commit('SET_STAFF_ALL_DATA', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res){
                        resolve(false)
                    }
                })
            })
        },
        initPositionsData({commit}) {
            return new Promise((resolve) => {
                ServiceStaff.initData('/hrtrain/Positions/?limit=2000').then((res) => {
                    if (res.data) {
                        commit('SET_POSITIONS_DATA', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                })
            })
        },
        initCardData({commit}) {
            return new Promise((resolve) => {
                ServiceStaff.initData('/hrtime/C_card/?limit=99999').then((res) => {
                    if (res.data) {
                        commit('SET_CARD_DATA', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                })
            })
        },
        initApproveMaster({commit}, data) {
            return new Promise((resolve) => {
                ServiceStaff.initData('/hrtrain/Approve_Project/active/1/perid/' + data.perid + '/?sort=Approve_Project_Type_Ms_Asc').then((res) => {
                    if (res.data) {
                        commit('SET_APPROVE_MASTER', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        resolve(true)
                    }
                })
            })
        },
        initApproveMasterByCordi({commit}, data) {
            return new Promise((resolve, reject) => {
                ServiceStaff.initData('/hrtrain/Approve_Project/active/1/perid/' + data.perid + '/type/' + data.type + '/?sort=Approve_Project_Type_Ms_Asc').then((res) => {
                    if (res.data) {
                        commit('SET_APPROVE_MASTER', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initApproveMasterByType({commit}, data) {
            return new Promise((resolve, reject) => {
                ServiceStaff.initData('/hrtrain/Approve_Project/active/1/type/' + data.type + '/?sort=Approve_Project_Type_Ms_Asc').then((res) => {
                    if (res.data) {
                        commit('SET_APPROVE_MASTER', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initApproveMasterAll({commit}) {
            return new Promise((resolve, reject) => {
                ServiceStaff.initData('/hrtrain/Approve_Project_view/active/1/?sort=Approve_Project_Type_Ms_Asc').then((res) => {
                    if (res.data) {
                        commit('SET_APPROVE_MASTER_ALL', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getExecCardData({commit}, data) {
            return new Promise((resolve) => {
                axios.get(ConstData.ConnectionHost + '/apiCardData/execCardDataJson/cardno/' + data.card_no).then((res) => {
                    if (res.data) {
                        commit('SET_CARD_DATA2', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        resolve(true)
                    }
                })
            })
        },
        getCardDataMedStudent({commit}, data) {
            return new Promise((resolve) => {
                axios.post(ConstData.ConnectionHost + '/HrTrain2/serviceAPI.php',{
                    queryName: 'MedStudentCardData',
                    param: btoa(encodeURIComponent(JSON.stringify({
                        cardId: parseInt(data.card_no)
                    })))
                }).then((res) => {
                    if (res.data) {
                        commit('SET_CARD_DATA2', res.data.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        resolve(false)
                    }
                })
            })
        },
        getAcademicFundsRemain({commit}, data) {
            return new Promise((resolve) => {
                axios.post(ConstData.ConnectionHost + '/HrTrain2/serviceAPI.php',{
                    queryName: 'getAcademicFundsRemain',
                    param: {
                        depart_id: data.depart_id
                    }
                }).then((res) => {
                    if (res.data) {
                        commit('SET_ACADEMIC_FUNDS', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        resolve(false)
                    }
                })
            })
        },
        initAcademicFundsByPerson({commit}, data) {
            return new Promise((resolve) => {
                axios.post(ConstData.ConnectionHost + '/HrTrain2/serviceAPI.php',{
                    queryName: 'initAcademicFundsByPerson',
                    param: {
                        perid: data.perid
                    }
                }).then((res) => {
                    if (res.data) {
                        commit('SET_ACADEMIC_FUNDS_BY_PERSON', res.data)
                        resolve(true)
                    } else {
                        resolve(true)
                    }
                }).catch(res => {
                    if (res) {
                        resolve(true)
                    }
                })
            })
        },
        saveSessions({commit}, data) {
            commit('saveSessions', data)
        },
        SendMail({commit}, data) {
            commit('SendMail', data)
        },
        TestFunc({commit}, data) {
            commit(true)
            // commit('TestFunc', data)
            console.log(data)
        }
    },
    getters: {
        getMeData(state) {
            return state.MeData
        },
        getAdminData(state) {
            return state.AdminData
        },
        getStaff(state) {
            return state.StaffData
        },
        getAllStaff(state) {
            return state.StaffAllData
        },
        getPosition(state) {
            return state.PositionsData
        },
        getCard(state) {
            return state.CardData
        },
        getCard2(state) {
            return state.CardData2
        },
        getApproveMaster(state) {
            return state.ApproveMaster
        },
        getAllApproveMaster(state) {
            return state.AllApproveMaster
        },
        getAcademicFunds(state) {
            return state.AcademicFunds
        },
        getAcademicFundsByPerson(state) {
            return state.AcademicFundsByPerson
        }
    }
}

function  parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    // console.log(JSON.parse(jsonPayload))
    // // return {perid: '1673'}
    // return {perid: '46792'}
    return JSON.parse(jsonPayload);
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export default staffModule
