import { CourseService } from '@/resource'

const courseModule = {
    namespaced: true,
    state: {
        courseData: [],
        courseGroupData: [],
        courseTypeData: []
    },
    mutations: {
        SET_COURSE_DATA(state, data) {
            if (data.length > 0) {
                state.courseData = data
            } else {
                state.courseData.push(data)
            }
        },
        SET_COURSEGROUP_DATA(state, data) {
            if (data.length > 0) {
                state.courseGroupData = data
            } else {
                state.courseGroupData.push(data)
            }
        },
        SET_COURSETYPE_DATA(state, data) {
            if (data.length > 0) {
                state.courseTypeData = data
            } else {
                state.courseTypeData.push(data)
            }
        }
    },
    actions: {
        initCourseData({ commit }) {
            return new Promise((resolve, reject) => {
                CourseService.initData('/hrtrain/D_course_view/?sort=Create_time_ASC&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_COURSE_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initCourseGroupData({ commit }){
            return new Promise((resolve, reject) => {
                CourseService.initData('/hrtrain/C_course_group/?limit=2000').then((res) => {
                    if(res.data){
                        commit('SET_COURSEGROUP_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initCourseTypeData({ commit }){
            return new Promise((resolve, reject) => {
                CourseService.initData('/hrtrain/C_course_type/?limit=2000').then((res) => {
                    if(res.data){
                        commit('SET_COURSETYPE_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        }
    },
    getters: {
        getCourse(state){
            return state.courseData
        },
        getCourseGroup(state){
            return state.courseGroupData
        },
        getCourseType(state){
            return state.courseTypeData
        }
    }
}

export default courseModule