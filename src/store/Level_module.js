import { LevelService } from '../resource'

const levelModule = {
    namespaced: true,
    state: {
        levelData: []
    },
    mutations: {
        SET_LEVEL_DATA(state, data) {
            if (data.length > 0) {
                state.levelData = data
            } else {
                state.levelData.push(data)
            }
        }
    },
    actions: {
        // initLevelData({ commit }) {
        //     return new Promise((resolve, reject) => {
        //         LevelService.request('GET', '/MedHr/app-api/v2/?/apis/mmis_staff/LevelCode').then((res) => {
        //             if(res.data.length > 0){
        //                 commit('SET_LEVEL_DATA', res.data)
        //                 resolve(true)
        //             } else {
        //                 reject(true)
        //             }
        //         })
        //     })
        // },
        initLevelData({ commit }) {
            return new Promise((resolve, reject) => {
                LevelService.initData('/mmis_staff/LevelCode').then((res) => {
                    if(res.data){
                        commit('SET_LEVEL_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
    },
    getters: {
        getLevel(state){
            return state.levelData
        }
    }
}

export default levelModule