import {DepartService} from '@/resource'

const departModule = {
    namespaced: true,
    state: {
        departData: []
    },
    mutations: {
        SET_DEPART_DATA(state, data) {
            state.departData = []
            if (data.length > 0) {
                state.departData = data
            } else {
                state.departData.push(data)
            }
        }
    },
    actions: {
        initDepartData({ commit }) {
            return new Promise((resolve, reject) => {
                DepartService.initData('/viewextdb/depart_view/dep_status/1/?limit=2000').then((res) => {
                    if(res.data){
                        commit('SET_DEPART_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
    },
    getters: {
        getDepart(state){
            return state.departData
        }
    }
}

export default departModule
