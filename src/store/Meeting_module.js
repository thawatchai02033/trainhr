import {ServiceProject} from '@/resource'

const MettingModule = {
    namespaced: true,
    state: {
        MeetingData: [],
        MeetingCordiData: [],
        JoinMeetingData: [],
        JoinMeetingDataByPerson: [],
        FileData: [],
    },
    mutations: {
        SET_MEETING_DATA(state, data) {
            state.MeetingData = []
            if (data.length >= 0) {
                state.MeetingData = data
            } else {
                state.MeetingData.push(data)
            }
        },
        SET_MEETING_CORDI_DATA(state, data) {
            state.MeetingData = []
            if (data.length >= 0) {
                state.MeetingCordiData = data
            } else {
                state.MeetingCordiData.push(data)
            }
        },
        SET_JOIN_MEETING_DATA(state, data) {
            state.JoinMeetingData = []
            if (data.length >= 0) {
                state.JoinMeetingData = data
            } else {
                state.JoinMeetingData.push(data)
            }
        },
        SET_JOIN_MEETING_DATA_BY_PERSON(state, data) {
            state.JoinMeetingDataByPerson = []
            if (data.length >= 0) {
                state.JoinMeetingDataByPerson = data
            } else {
                state.JoinMeetingDataByPerson.push(data)
            }
        },
        SET_FILE_DATA(state, data) {
            state.FileData = []
            if (data.length >= 0) {
                state.FileData = data
            } else {
                state.FileData.push(data)
            }
        },
    },
    actions: {
        initMeetingData({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_meeting_view/active/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        if (res.data.length > 0) {
                            var countCordi = 0
                            var target = []
                            var positionCordi = []
                            var departCordi = []
                            res.data.map(item => {
                                target = []
                                positionCordi = []
                                departCordi = []
                                if (item.Cordi == '1') {
                                    ServiceProject.initData('/hrtrain/D_meeting_target/Ref_id/' + item.Ref_id + '/active/1/?sort=id_Desc&limit=9999').then((resTarget) => {
                                        if (resTarget.data) {
                                            if (resTarget.data.length > 0) {
                                                target = resTarget.data
                                            } else {
                                                target.push(resTarget.data)
                                            }

                                            ServiceProject.initData('/hrtrain/C_person_level/D_meeting_target_Ref_id/' + item.Ref_id + '/active/1?limit=9999').then((resPosi) => {
                                                if (resPosi.data) {
                                                    if (resPosi.data.length > 0) {
                                                        positionCordi = resPosi.data
                                                    } else {
                                                        positionCordi.push(resPosi.data)
                                                    }

                                                    ServiceProject.initData('/hrtrain/quata_dep_reg/D_meeting_target_Ref_id/' + item.Ref_id + '/active/1/?sort=id_Desc&limit=9999').then((resDep) => {
                                                        if (resDep.data) {
                                                            if (resDep.data.length > 0) {
                                                                departCordi = resDep.data
                                                            } else {
                                                                departCordi.push(resDep.data)
                                                            }
                                                        }
                                                        countCordi++
                                                        item.Cordi_Join_Data = {
                                                            target: target,
                                                            position: positionCordi,
                                                            depart: departCordi
                                                        }

                                                        if (countCordi == res.data.length) {
                                                            commit('SET_MEETING_DATA', res.data)
                                                            resolve(true)
                                                        }
                                                    })
                                                } else {
                                                    ServiceProject.initData('/hrtrain/quata_dep_reg/D_meeting_target_Ref_id/' + item.Ref_id + '/?sort=id_Desc&limit=9999').then((resDep) => {
                                                        if (resDep.data) {
                                                            if (resDep.data.length > 0) {
                                                                departCordi = resDep.data
                                                            } else {
                                                                departCordi.push(resDep.data)
                                                            }
                                                        }
                                                        countCordi++
                                                        item.Cordi_Join_Data = {
                                                            target: target,
                                                            position: positionCordi,
                                                            depart: departCordi
                                                        }

                                                        if (countCordi == res.data.length) {
                                                            commit('SET_MEETING_DATA', res.data)
                                                            resolve(true)
                                                        }
                                                    })
                                                }
                                            })
                                        } else {
                                            countCordi++
                                            item.Cordi_Join_Data = {
                                                target: target,
                                                position: positionCordi,
                                                depart: departCordi
                                            }

                                            if (countCordi == res.data.length) {
                                                commit('SET_MEETING_DATA', res.data)
                                                resolve(true)
                                            }
                                        }
                                    })
                                } else {
                                    countCordi++
                                    item.Cordi_Join_Data = {
                                        target: target,
                                        position: positionCordi,
                                        depart: departCordi
                                    }

                                    if (countCordi == res.data.length) {
                                        commit('SET_MEETING_DATA', res.data)
                                        resolve(true)
                                    }
                                }
                            })
                        }
                    } else {
                        commit('SET_MEETING_DATA', [])
                        reject(true)
                    }
                })
            })
        },
        getMeetingData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_meeting_view/uuid/' + data.uuid + '/active/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        var countCordi = 0
                        var target = []
                        var positionCordi = []
                        var departCordi = []
                        if (res.data.length > 0) {
                            res.data.map(item => {
                                target = []
                                positionCordi = []
                                departCordi = []
                                if (item.Cordi == '1') {
                                    ServiceProject.initData('/hrtrain/D_meeting_target/Ref_id/' + item.Ref_id + '/active/1/?sort=id_Desc&limit=9999').then((resTarget) => {
                                        if (resTarget.data) {
                                            if (resTarget.data.length > 0) {
                                                target = resTarget.data
                                            } else {
                                                target.push(resTarget.data)
                                            }

                                            ServiceProject.initData('/hrtrain/C_person_level/D_meeting_target_Ref_id/' + item.Ref_id + '/active/1?limit=9999').then((resPosi) => {
                                                if (resPosi.data) {
                                                    if (resPosi.data.length > 0) {
                                                        positionCordi = resPosi.data
                                                    } else {
                                                        positionCordi.push(resPosi.data)
                                                    }

                                                    ServiceProject.initData('/hrtrain/quata_dep_reg/D_meeting_target_Ref_id/' + item.Ref_id + '/active/1/?sort=id_Desc&limit=9999').then((resDep) => {
                                                        if (resDep.data) {
                                                            if (resDep.data.length > 0) {
                                                                departCordi = resDep.data
                                                            } else {
                                                                departCordi.push(resDep.data)
                                                            }
                                                        }
                                                        countCordi++
                                                        item.Cordi_Join_Data = {
                                                            target: target,
                                                            position: positionCordi,
                                                            depart: departCordi
                                                        }

                                                        if (countCordi == res.data.length) {
                                                            commit('SET_MEETING_DATA', res.data)
                                                            resolve(true)
                                                        }
                                                    })
                                                } else {
                                                    ServiceProject.initData('/hrtrain/quata_dep_reg/D_meeting_target_Ref_id/' + item.Ref_id + '/?sort=id_Desc&limit=9999').then((resDep) => {
                                                        if (resDep.data) {
                                                            if (resDep.data.length > 0) {
                                                                departCordi = resDep.data
                                                            } else {
                                                                departCordi.push(resDep.data)
                                                            }
                                                        }
                                                        countCordi++
                                                        item.Cordi_Join_Data = {
                                                            target: target,
                                                            position: positionCordi,
                                                            depart: departCordi
                                                        }

                                                        if (countCordi == res.data.length) {
                                                            commit('SET_MEETING_DATA', res.data)
                                                            resolve(true)
                                                        }
                                                    })
                                                }
                                            })
                                        } else {
                                            countCordi++
                                            item.Cordi_Join_Data = {
                                                target: target,
                                                position: positionCordi,
                                                depart: departCordi
                                            }

                                            if (countCordi == res.data.length) {
                                                commit('SET_MEETING_DATA', res.data)
                                                resolve(true)
                                            }
                                        }
                                    })
                                } else {
                                    countCordi++
                                    item.Cordi_Join_Data = {
                                        target: target,
                                        position: positionCordi,
                                        depart: departCordi
                                    }

                                    if (countCordi == res.data.length) {
                                        commit('SET_MEETING_DATA', res.data)
                                        resolve(true)
                                    }
                                }
                            })
                        } else {
                            if (res.data.Cordi == '1') {
                                ServiceProject.initData('/hrtrain/D_meeting_target/Ref_id/' + res.data.Ref_id + '/active/1/?sort=id_Desc&limit=9999').then((resTarget) => {
                                    if (resTarget.data) {
                                        if (resTarget.data.length > 0) {
                                            target = resTarget.data
                                        } else {
                                            target.push(resTarget.data)
                                        }

                                        ServiceProject.initData('/hrtrain/C_person_level/D_meeting_target_Ref_id/' + res.data.Ref_id + '/active/1?limit=9999').then((resPosi) => {
                                            if (resPosi.data) {
                                                if (resPosi.data.length > 0) {
                                                    positionCordi = resPosi.data
                                                } else {
                                                    positionCordi.push(resPosi.data)
                                                }

                                                ServiceProject.initData('/hrtrain/quata_dep_reg/D_meeting_target_Ref_id/' + res.data.Ref_id + '/active/1/?sort=id_Desc&limit=9999').then((resDep) => {
                                                    if (resDep.data) {
                                                        if (resDep.data.length > 0) {
                                                            departCordi = resDep.data
                                                        } else {
                                                            departCordi.push(resDep.data)
                                                        }
                                                    }
                                                    res.data.Cordi_Join_Data = {
                                                        target: target,
                                                        position: positionCordi,
                                                        depart: departCordi
                                                    }

                                                    commit('SET_MEETING_DATA', res.data)
                                                    resolve(true)
                                                })
                                            } else {
                                                ServiceProject.initData('/hrtrain/quata_dep_reg/D_meeting_target_Ref_id/' + res.data.Ref_id + '/?sort=id_Desc&limit=9999').then((resDep) => {
                                                    if (resDep.data) {
                                                        if (resDep.data.length > 0) {
                                                            departCordi = resDep.data
                                                        } else {
                                                            departCordi.push(resDep.data)
                                                        }
                                                    }
                                                    res.data.Cordi_Join_Data = {
                                                        target: target,
                                                        position: positionCordi,
                                                        depart: departCordi
                                                    }

                                                    commit('SET_MEETING_DATA', res.data)
                                                    resolve(true)
                                                })
                                            }
                                        })
                                    } else {
                                        res.data.Cordi_Join_Data = {
                                            target: target,
                                            position: positionCordi,
                                            depart: departCordi
                                        }

                                        commit('SET_MEETING_DATA', res.data)
                                        resolve(true)
                                    }
                                })
                            } else {

                                res.data.Cordi_Join_Data = {
                                    target: target,
                                    position: positionCordi,
                                    depart: departCordi
                                }

                                commit('SET_MEETING_DATA', res.data)
                                resolve(true)
                            }
                        }
                    } else {
                        commit('SET_MEETING_DATA', [])
                        reject(true)
                    }
                })
            })
        },
        initJoinMeetingData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_meeting_join_view2/D_meeting_uuid/' + data.uuid + '/status/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_JOIN_MEETING_DATA', res.data)
                        resolve(true)
                    } else {
                        commit('SET_JOIN_MEETING_DATA', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_JOIN_MEETING_DATA', [])
                        reject(true)
                    }
                })
            })
        },
        getJoinMeetingDataByPerson({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_meeting_join_view2/perid/' + data.perid + '/status/1/status_reg/1/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_JOIN_MEETING_DATA_BY_PERSON', res.data)
                        resolve(true)
                    } else {
                        commit('SET_JOIN_MEETING_DATA_BY_PERSON', [])
                        reject(true)
                    }
                })
            })
        },
        initFileData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/train_files/active/1/Ref_id/' + data.Ref_id + '/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_FILE_DATA', res.data)
                        resolve(true)
                    } else {
                        commit('SET_FILE_DATA', [])
                        reject(true)
                    }
                })
            })
        },
        initMeetingCordiData({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_meeting_cordi/active/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_MEETING_CORDI_DATA', res.data)
                        resolve(true)
                    } else {
                        commit('SET_MEETING_CORDI_DATA', [])
                        reject(true)
                    }
                })
            })
        }
    },
    getters: {
        getMeetingData(state) {
            return state.MeetingData
        },
        getMeetingCordiData(state) {
            return state.MeetingCordiData
        },
        getJoinMeetingData(state) {
            return state.JoinMeetingData
        },
        getJoinMeetingDataByPerson(state) {
            return state.JoinMeetingDataByPerson
        },
        getFileData(state) {
            return state.FileData
        },
    }
}

export default MettingModule
