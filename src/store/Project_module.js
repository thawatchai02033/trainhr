import {ServiceProject} from '@/resource'
import ConstData from "@/const/Const";
import axios from "axios";

const projectModule = {
    namespaced: true,
    state: {
        Approve_Project: [],
        ProjectRecord: [],
        ProjectData: [],
        ProjectDataApprove: [],
        CommitteeData: [],
        CommitteeType: [],
        ProvincesData: [],
        CountryData: [],
        ExpenseData: [],
        ProjectExpenseData: [],
        CompetencyData: [],
        ProjectGroupData: [],
        recordProjectData: [],
        TextForPrint: '',
        boardOption: [],
        TrainHistory: [],
        PresentHistory: [],
        CommitHistory: [],
        historyExpert: [],
        registerProjectData: [],
        requestProjectData: [],
        JournalData: [],
        AwardData: [],
        Train_Files: [],
        QuotaDepart: [],
        ProjectRegData: [],
        joinProjectData: [],
        GroupExamProject: [],
        ProjectPlanData: [],
        PlanProjectDataApprove: [],
        PlanProjectDataHistory: [],
        WorkExpData: [],
        CourseWorkExp: [],
        Train_Ms: [],
        Committee_Ms: [],
        Organizer_Ms: [],
        Presentation_Status_Ms: [],
        Project_Place_Ms: [],
        Group_Quota_Ms: [],
        Group_Quota: [],
        Project_RoundData: [],
        LecturerData: [],
        TemplateProjectData: [],
        ProjectFiles: [],
        ApproveForm: [],
        PersonFiles: [],
        UrlRegister: [],
        DateForPlan: [],
        Log_project: [],
        Log_competency: [],
        Log_project_file: [],
        Temp_project: [],
        Temp_competency: [],
        Temp_project_file: [],
        TypeProject: null,
        ApproveHR: null,
        HeaderPage: null,
        PlanProjectPage: null,
        ManageProjectPage: null,
        JoinProjectPage: null,
        StatusProject: [],
        TempStatusProject: [],
        TravelFrom: [],
        JoinTravel: [],
        ProjectExportData: [],
        ProjectRegListData: [],
    },
    mutations: {
        SET_PROJECT_ROUND_DATA(state, data) {
            state.Project_RoundData = []
            if (data.length >= 0) {
                state.Project_RoundData = data
            } else {
                if (data != null) {
                    state.Project_RoundData.push(data)
                }
            }
        },
        SET_GROUP_QUOTA_MS(state, data) {
            state.Group_Quota_Ms = []
            if (data.length >= 0) {
                state.Group_Quota_Ms = data
            } else {
                if (data != null) {
                    state.Group_Quota_Ms.push(data)
                }
            }
        },
        SET_GROUP_QUOTA(state, data) {
            state.Group_Quota = []
            if (data.length >= 0) {
                state.Group_Quota = data
            } else {
                state.Group_Quota.push(data)
            }
        },
        SET_PROJECT_PLACE_MS(state, data) {
            state.Project_Place_Ms = []
            if (data.length >= 0) {
                state.Project_Place_Ms = data
            } else {
                state.Project_Place_Ms.push(data)
            }
        },
        SET_TRAIN_MS(state, data) {
            state.Train_Ms = []
            if (data.length >= 0) {
                state.Train_Ms = data
            } else {
                state.Train_Ms.push(data)
            }
        },
        SET_COMMITTEE_MS(state, data) {
            state.Committee_Ms = []
            if (data.length >= 0) {
                state.Committee_Ms = data
            } else {
                state.Committee_Ms.push(data)
            }
        },
        SET_ORGANIZER_MS(state, data) {
            state.Organizer_Ms = []
            if (data.length >= 0) {
                state.Organizer_Ms = data
            } else {
                state.Organizer_Ms.push(data)
            }
        },
        SET_PRESENTATION_STATUS_MS(state, data) {
            state.Presentation_Status_Ms = []
            if (data.length >= 0) {
                state.Presentation_Status_Ms = data
            } else {
                state.Presentation_Status_Ms.push(data)
            }
        },
        SET_COURSEWORKEXP_DATA(state, data) {
            state.CourseWorkExp = []
            if (data.length >= 0) {
                state.CourseWorkExp = data
            } else {
                state.CourseWorkExp.push(data)
            }
        },
        SET_WORKEXP_DATA(state, data) {
            state.WorkExpData = []
            if (data.length >= 0) {
                state.WorkExpData = data
            } else {
                state.WorkExpData.push(data)
            }
        },
        SET_PROJECT_PLAN_DATA(state, data) {
            state.ProjectPlanData = []
            if (data.length >= 0) {
                state.ProjectPlanData = data
            } else {
                state.ProjectPlanData.push(data)
            }
        },
        SET_GROUPEXAMPROJECT_DATA(state, data) {
            state.GroupExamProject = []
            if (data.length >= 0) {
                state.GroupExamProject = data
            } else {
                state.GroupExamProject.push(data)
            }
        },
        SET_JOINPROJECT_DATA(state, data) {
            state.joinProjectData = []
            if (data.length >= 0) {
                state.joinProjectData = data
            } else {
                state.joinProjectData.push(data)
            }
        },
        SET_QUOTA_DEPART_DATA(state, data) {
            state.QuotaDepart = []
            if (data.length >= 0) {
                state.QuotaDepart = data
            } else {
                state.QuotaDepart.push(data)
            }
        },
        SET_PROJECTREG_DATA(state, data) {
            state.ProjectRegData = []
            if (data.length >= 0) {
                state.ProjectRegData = data
            } else {
                state.ProjectRegData.push(data)
            }
        },
        SET_TRAIN_FILES_DATA(state, data) {
            state.Train_Files = []
            if (data.length >= 0) {
                state.Train_Files = data
            } else {
                state.Train_Files.push(data)
            }
        },
        SET_AWARD_DATA(state, data) {
            state.AwardData = []
            if (data.length >= 0) {
                state.AwardData = data
            } else {
                state.AwardData.push(data)
            }
        },
        SET_JOURNAL_DATA(state, data) {
            state.JournalData = []
            if (data.length >= 0) {
                state.JournalData = data
            } else {
                state.JournalData.push(data)
            }
        },
        SET_REGISTER_PROJECT_DATA(state, data) {
            state.registerProjectData = []
            if (data.length >= 0) {
                state.registerProjectData = data
            } else {
                state.registerProjectData.push(data)
            }
        },
        SET_REQUEST_PROJECT_DATA(state, data) {
            state.requestProjectData = []
            if (data.length >= 0) {
                state.requestProjectData = data
            } else {
                state.requestProjectData.push(data)
            }
        },
        SET_HISTORYEXPERT_DATA(state, data) {
            state.historyExpert = []
            if (data.length >= 0) {
                state.historyExpert = data
            } else {
                state.historyExpert.push(data)
            }
        },
        SET_BOARDOPTION_DATA(state, data) {
            state.boardOption = []
            if (data.length >= 0) {
                state.boardOption = data
            } else {
                state.boardOption.push(data)
            }
        },
        SET_TRAINHISTORY_DATA(state, data) {
            state.TrainHistory = []
            if (data.length >= 0) {
                state.TrainHistory = data
            } else {
                state.TrainHistory.push(data)
            }
        },
        SET_COMMITHISTORY_DATA(state, data) {
            state.CommitHistory = []
            if (data.length >= 0) {
                state.CommitHistory = data
            } else {
                state.CommitHistory.push(data)
            }
        },
        SET_PRESENTHISTORY_DATA(state, data) {
            state.PresentHistory = []
            if (data.length >= 0) {
                state.PresentHistory = data
            } else {
                state.PresentHistory.push(data)
            }
        },
        SET_TEXTFORPRINT(state, data) {
            state.TextForPrint = ''
            state.TextForPrint = data
        },
        SET_APPROVE_PROJECT_DATA(state, data) {
            state.Approve_Project = []
            if (data.length >= 0) {
                state.Approve_Project = data
            } else {
                state.Approve_Project.push(data)
            }
        },
        SET_PROJECTRECORD_DATA(state, data) {
            state.ProjectRecord = []
            if (data.length >= 0) {
                state.ProjectRecord = data
            } else {
                state.ProjectRecord.push(data)
            }
        },
        SET_PROJECT_DATA(state, data) {
            state.ProjectData = []
            if (data.length >= 0) {
                state.ProjectData = data
            } else {
                state.ProjectData.push(data)
            }
        },
        SET_PROJECT_DATA_APPROVE(state, data) {
            state.ProjectDataApprove = []
            if (data.length >= 0) {
                state.ProjectDataApprove = data
            } else {
                state.ProjectDataApprove.push(data)
            }
        },
        SET_EXPORT_LIST_DATA(state, data) {
            state.ProjectExportData = []
            if (data.length >= 0) {
                state.ProjectExportData = data
            } else {
                state.ProjectExportData.push(data)
            }
        },
        SET_PROJ_REG_LIST_DATA(state, data) {
            state.ProjectRegListData = []
            if (data.length >= 0) {
                state.ProjectRegListData = data
            } else {
                state.ProjectRegListData.push(data)
            }
        },
        SET_PLAN_PROJECT_DATA_APPROVE(state, data) {
            state.PlanProjectDataApprove = []
            if (data.length >= 0) {
                state.PlanProjectDataApprove = data
            } else {
                state.PlanProjectDataApprove.push(data)
            }
        },
        SET_PLAN_PROJECT_DATA_HISTORY(state, data) {
            state.PlanProjectDataHistory = []
            if (data.length >= 0) {
                state.PlanProjectDataHistory = data
            } else {
                state.PlanProjectDataHistory.push(data)
            }
        },
        SET_COMMITTEEDATA_DATA(state, data) {
            state.CommitteeData = []
            if (data.length >= 0) {
                state.CommitteeData = data
            } else {
                state.CommitteeData.push(data)
            }
        },
        SET_COMMITTEEDATA_TYPE(state, data) {
            state.CommitteeType = []
            if (data.length >= 0) {
                state.CommitteeType = data
            } else {
                state.CommitteeType.push(data)
            }
        },
        SET_PROVINCES_DATA(state, data) {
            state.ProvincesData = []
            if (data.length >= 0) {
                state.ProvincesData = data
            } else {
                state.ProvincesData.push(data)
            }
        },
        SET_COUNTRY_DATA(state, data) {
            state.CountryData = []
            if (data.length >= 0) {
                state.CountryData = data
            } else {
                state.CountryData.push(data)
            }
        },
        SET_EXPENSE_DATA(state, data) {
            state.ExpenseData = []
            if (data.length >= 0) {
                state.ExpenseData = data
            } else {
                state.ExpenseData.push(data)
            }
        },
        SET_PEOJECTEXPENSE_DATA(state, data) {
            state.ProjectExpenseData = []
            if (data.length >= 0) {
                state.ProjectExpenseData = data
            } else {
                state.ProjectExpenseData.push(data)
            }
        },
        SET_COMPETENCY_DATA(state, data) {
            state.CompetencyData = []
            if (data.length >= 0) {
                state.CompetencyData = data
            } else {
                state.CompetencyData.push(data)
            }
        },
        SET_PROJECTGROUP_DATA(state, data) {
            state.ProjectGroupData = []
            if (data.length >= 0) {
                state.ProjectGroupData = data
            } else {
                state.ProjectGroupData.push(data)
            }
        },
        SET_LECTURER_DATA(state, data) {
            state.LecturerData = []
            if (data.length >= 0) {
                state.LecturerData = data
            } else {
                state.LecturerData.push(data)
            }
        },
        SET_TEMPLATEPROJECT_DATA(state, data) {
            state.TemplateProjectData = []
            if (data.length >= 0) {
                state.TemplateProjectData = data
            } else {
                state.TemplateProjectData.push(data)
            }
        },
        SET_PROJECTFILE_DATA(state, data) {
            state.ProjectFiles = []
            if (data.length >= 0) {
                state.ProjectFiles = data
            } else {
                state.ProjectFiles.push(data)
            }
        },
        SET_PERSONFILE_DATA(state, data) {
            state.PersonFiles = []
            if (data.length >= 0) {
                state.PersonFiles = data
            } else {
                state.PersonFiles.push(data)
            }
        },
        SET_APPROVE_FORM(state, data) {
            state.ApproveForm = []
            if (data.length >= 0) {
                state.ApproveForm = data
            } else {
                state.ApproveForm.push(data)
            }
        },
        SET_URLREGISTER(state, data) {
            state.UrlRegister = []
            if (data.length >= 0) {
                state.UrlRegister = data
            } else {
                state.UrlRegister.push(data)
            }
        },
        SET_DATEFORPLAN(state, data) {
            state.DateForPlan = []
            if (data.length >= 0) {
                state.DateForPlan = data
            } else {
                state.DateForPlan.push(data)
            }
        },
        SET_LOG_PROJECT(state, data) {
            state.Log_project = []
            if (data.length >= 0) {
                state.Log_project = data
            } else {
                state.Log_project.push(data)
            }
        },
        SET_LOG_COMPETENCY(state, data) {
            state.Log_competency = []
            if (data.length >= 0) {
                state.Log_competency = data
            } else {
                state.Log_competency.push(data)
            }
        },
        SET_LOG_PROJECT_FILE(state, data) {
            state.Log_project_file = []
            if (data.length >= 0) {
                state.Log_project_file = data
            } else {
                state.Log_project_file.push(data)
            }
        },
        SET_TEMP_PROJECT(state, data) {
            state.Temp_project = []
            if (data.length >= 0) {
                state.Temp_project = data
            } else {
                state.Temp_project.push(data)
            }
        },
        SET_TEMP_COMPETENCY(state, data) {
            state.Temp_competency = []
            if (data.length >= 0) {
                state.Temp_competency = data
            } else {
                state.Temp_competency.push(data)
            }
        },
        SET_TEMP_PROJECT_FILE(state, data) {
            state.Temp_project_file = []
            if (data.length >= 0) {
                state.Temp_project_file = data
            } else {
                state.Temp_project_file.push(data)
            }
        },
        SET_STATUS_PROJECT(state, data) {
            state.StatusProject = []
            if (data.length >= 0) {
                state.StatusProject = data
            } else {
                state.StatusProject.push(data)
            }
        },
        SET_TEMP_STATUS_PROJECT(state, data) {
            state.TempStatusProject = []
            if (data.length >= 0) {
                state.TempStatusProject = data
            } else {
                state.TempStatusProject.push(data)
            }
        },
        SET_TRAVEL_FORM(state, data) {
            state.TravelFrom = []
            if (data.length >= 0) {
                state.TravelFrom = data
            } else {
                state.TravelFrom.push(data)
            }
        },
        SET_JOIN_TRAVEL(state, data) {
            state.JoinTravel = []
            if (data.length >= 0) {
                state.JoinTravel = data
            } else {
                state.JoinTravel.push(data)
            }
        },
        SET_TYPE_PROJECT(state, data) {
            state.TypeProject = data
        },
        SET_APPROVE_BY_HR(state, data) {
            state.ApproveHR = data
        },
        SET_HEADER_PAGE(state, data) {
            state.HeaderPage = data
        },
        SET_PLAN_PROJECT_PAGE(state, data) {
            state.PlanProjectPage = data
        },
        SET_MANAGE_PROJECT_PAGE(state, data) {
            state.ManageProjectPage = data
        },
        SET_JOIN_PROJECT_PAGE(state, data) {
            state.JoinProjectPage = data
        }
    },
    actions: {
        initProjectRoundData({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_round_view/active/1/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PROJECT_ROUND_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        getProjectRoundData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_round_view/Project_code/' + data.Project_code + '/active/1/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PROJECT_ROUND_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(reason => {
                    if (reason) {
                        reject(true)
                    }
                })
            })
        },
        getProjectRoundDataById({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_round_view/id/' + data.id + '/Project_code/' + data.Project_code + '/active/1/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PROJECT_ROUND_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(reason => {
                    if (reason) {
                        reject(true)
                    }
                })
            })
        },
        initProjectPlace_Ms({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/project_place_ms/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PROJECT_PLACE_MS', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initGroup_Quota_Ms({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/group_quota_ms/active/1/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_GROUP_QUOTA_MS', res.data)
                        resolve(true)
                    } else {
                        commit('SET_GROUP_QUOTA_MS', null)
                        reject(true)
                    }
                })
            })
        },
        initGroup_Quota({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/group_quota/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_GROUP_QUOTA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initTrain_Ms({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/E_Train_Ms/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_TRAIN_MS', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initOrganizer_Ms({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/E_Organizer_Ms/?sort=organ_id_DESC&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_ORGANIZER_MS', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initCommittee_Ms({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/E_Committee_Ms/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_COMMITTEE_MS', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initPresentation_Status_Ms({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/E_Presentation_Status_Ms/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PRESENTATION_STATUS_MS', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initCourseWorkExp({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_Course_Delv_view/C_workExp_id/' + data.C_workExp_id + '/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_COURSEWORKEXP_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initWorkExp({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/C_workExp_view/PosNew/' + data.PosNew + '/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_WORKEXP_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initGroupExamProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/C_person_level_view/Project_code/' + data.Project_code + '/active/1/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_GROUPEXAMPROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getGroupExamProjectByCordi({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/C_person_level_view/Project_code/' + data.Project_code + '/active/1/D_project_round_id/' + data.id + '/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_GROUPEXAMPROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initProjectRegister({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_Project_Register_view/Dep_Code/' + data.dep_id + '/active/1/?sort=D_Project_Register_id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PROJECTREG_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initAllProjectRegister({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_Project_Register_view/active/1/?sort=D_Project_Register_id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PROJECTREG_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        getQuotaDepartRegisterByCordi({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/quata_dep_reg/Project_code/' + data.Project_code + '/D_project_round_id/' + data.id + '/active/1/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_QUOTA_DEPART_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initQuotaDepartRegister({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/quata_dep_reg/active/1/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_QUOTA_DEPART_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initTrainFilesProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/train_files/active/1/Ref_id/' + data.Ref_id + '/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_TRAIN_FILES_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initAwardProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/viewextdb/Award/FacID/' + data.perid + '/active/1/?sort=RefID_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_AWARD_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initAllAwardProject({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/viewextdb/Award_view/active/1/?sort=RefID_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_AWARD_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initJournalProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/viewextdb/Journal/active/1/FacID/' + data.perid + '/?sort=RefID_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_JOURNAL_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initAllJournalProject({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/viewextdb/Journal_view/active/1/?sort=RefID_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_JOURNAL_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        getJoinProjectByCordi({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/join_project_view/Project_code/' + data.Project_code + '/D_project_round_id/' + data.id + '/active/1/sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_JOINPROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        getJoinProjectByCordiAndUid({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/join_project_view/Project_code/' + data.Project_code + '/D_project_round_id/' + data.id + '/active/1/uid/' + data.perid + '/sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_JOINPROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        getJoinProjectByUid({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/join_project_view/Project_code/' + data.Project_code + '/uid/' + data.perid + '/active/1/sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_JOINPROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initJoinProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/join_project_view/Project_code/' + data.Project_code + '/active/1/sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_JOINPROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initRegisterProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/register_project_view/uid/' + data.perid + '/active/1/status_reg/1/sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_REGISTER_PROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initRequestProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/request_project_view/Project_code/' + data.Project_code + '/active_reg/1/sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_REQUEST_PROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        commit('SET_REQUEST_PROJECT_DATA', [])
                        reject(true)
                    }
                }).catch(reason => {
                    if (reason) {
                        reject(true)
                    }
                })
            })
        },
        initRequestProjectByRoundID({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/request_project_view/Project_code/' + data.Project_code + '/D_project_round_id/' + data.id + '/active_reg/1/sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_REQUEST_PROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        commit('SET_REQUEST_PROJECT_DATA', [])
                        reject(true)
                    }
                }).catch(reason => {
                    if (reason) {
                        commit('SET_REQUEST_PROJECT_DATA', [])
                        reject(true)
                    }
                })
            })
        },
        initRequestProjectByEmp({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/request_project_view/Project_code/' + data.Project_code + '/perid/' + data.perid + '/active_reg/1/sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_REQUEST_PROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initHistoryExperts({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/History_Experts_view2/active/1/perid/' + data.perid + '/sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_HISTORYEXPERT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initAllHistoryExperts({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/History_Experts_view2/active/1/sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_HISTORYEXPERT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initBoardOption({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/boardOption/' + data.perid).then((res) => {
                    if(res.data){
                        commit('SET_BOARDOPTION_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initCommitHistory({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/viewextdb/Commit/FacID/' + data.perid + '/?sort=RefID_desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_COMMITHISTORY_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initAllCommitHistory({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.CustomData('GET',ConstData.ConnectionHost + '/HrTrain2/Export.php?start=' + data.date_start + '&end=' + data.date_end + '&type=' + data.type,'').then((res) => {
                    if(res.data){
                        commit('SET_COMMITHISTORY_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject({
                            status: true,
                            message: 'ไม่สามารถ Export ข้อมูลได้ เนื่องจากจำนวนปริมาณข้อมูลมีขนาดใหญ่ โปรดเลือกช่วงวันที่ในการ Export ข้อมูลอีกครั้ง'
                        })
                    }
                })
            })
        },
        initPresentHistory({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/viewextdb/Presentation_view/FacID/' + data.perid + '/?sort=RefID_desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PRESENTHISTORY_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initAllPresentHistory({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.CustomData('GET',ConstData.ConnectionHost + '/HrTrain2/Export.php?start=' + data.date_start + '&end=' + data.date_end + '&type=' + data.type,'').then((res) => {
                    if(res.data){
                        commit('SET_PRESENTHISTORY_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject({
                            status: true,
                            message: 'ไม่สามารถ Export ข้อมูลได้ เนื่องจากจำนวนปริมาณข้อมูลมีขนาดใหญ่ โปรดเลือกช่วงวันที่ในการ Export ข้อมูลอีกครั้ง'
                        })
                    }
                })
            })
        },
        initTrainHistory({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/viewextdb/train_view/FacID/' + data.perid + '/?sort=RefID_desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_TRAINHISTORY_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initAllTrainHistory({ commit }, data) {
            return new Promise((resolve, reject) => {
                // axios.get('https://medhr.medicine.psu.ac.th/HrTrain2/Export.php?start=' + data.date_start + '&end=' + data.date_end).then((res) => {
                ServiceProject.CustomData('GET',ConstData.ConnectionHost + '/HrTrain2/Export.php?start=' + data.date_start + '&end=' + data.date_end + '&type=' + data.type,'').then((res) => {
                    if(res.data){
                        commit('SET_TRAINHISTORY_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                  if (res) {
                      reject({
                          status: true,
                          message: 'ไม่สามารถ Export ข้อมูลได้ เนื่องจากจำนวนปริมาณข้อมูลมีขนาดใหญ่ โปรดเลือกช่วงวันที่ในการ Export ข้อมูลอีกครั้ง'
                      })
                  }
                })
            })
        },
        MangeTextForPrint({ commit }, data) {
            return new Promise((resolve) => {
                if (data.process == 'GET') {
                    commit('SET_TEXTFORPRINT', '')
                    resolve(true)
                } else {
                    commit('SET_TEXTFORPRINT', data.message)
                    resolve(true)
                }
            })
        },
        initApproveProjectData({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Approve_Project/').then((res) => {
                    if(res.data){
                        commit('SET_APPROVE_PROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        getApproveProjectData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Approve_Project/perid/' + data.perid + '/type/' + data.type).then((res) => {
                    if(res.data){
                        commit('SET_APPROVE_PROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initProjectRecordData({ commit }, data) {
            return new Promise((resolve, reject) => {
                // ServiceProject.initData('/hrtrain/register_project/uid/' + data.perid).then((res) => {
                ServiceProject.initData('/hrtrain/D_project_record_view/Project_code/' + data.Project_code + '/?sort=id_Desc&limit=1').then((res) => {
                    if(res.data){
                        if (res.data.Status == '7') {
                            commit('SET_PROJECTRECORD_DATA', res.data)
                            resolve(true)
                        } else {
                            resolve(false)
                        }
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initAllProjectRecordData({ commit }, data) {
            return new Promise((resolve, reject) => {
                // ServiceProject.initData('/hrtrain/register_project/uid/' + data.perid).then((res) => {
                ServiceProject.initData('/hrtrain/D_project_record_view/Project_code/' + data.Project_code + '/Type/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PROJECTRECORD_DATA', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initProjectPlanData({ commit }, data) {
            return new Promise((resolve, reject) => {
                // ServiceProject.initData('/hrtrain/D_project_view/IsPlanStatus/2/active/1/Create_By/' + data.perid + '/?limit=5000').then((res) => {
                ServiceProject.initData('/hrtrain/D_project_view/active/1/Create_By/' + data.perid + '/?limit=5000').then((res) => {
                    if(res.data){
                        commit('SET_PROJECT_PLAN_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initProjectDataByCode({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_view/active/1/Project_code/' + data.Project_code + '/?limit=5000').then((res) => {
                    if(res.data){
                        commit('SET_PROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initProjectDataByUUID({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_view/active/1/uuid/' + data.uuid + '/?limit=5000').then((res) => {
                    if(res.data){
                        commit('SET_PROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initProjectData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_view/active/1/Create_By/' + data.perid + '/?sort=project_id_Desc&limit=99999').then((res) => {
                    if(res.data){
                        commit('SET_PROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initProjectDataForApprove({ commit }, data) {
            return new Promise((resolve, reject) => {
                // ServiceProject.initData('/hrtrain/D_project_view/IsPlan/0/active/1/?sort=project_id_Desc&limit=5000').then((res) => {
                // ServiceProject.initData('/hrtrain/D_project_view/active/1/?sort=project_id_Desc&limit=99999')
                axios.post(ConstData.ConnectionHost + '/HrTrain2/serviceAPI.php', {
                    queryName: 'getAllProject',
                    param: btoa(JSON.stringify({
                        perid: data.perid
                    }))
                }).then((res) => {
                    if(res.data){
                        commit('SET_PROJECT_DATA_APPROVE', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(resErr => {
                    if (resErr) {
                        resolve(false)
                    }
                })
            })
        },
        initExportList({ commit }, data) {
            return new Promise((resolve, reject) => {
                axios.post(ConstData.ConnectionHost + '/HrTrain2/serviceAPI2.php', {
                    queryName: 'export_list_data',
                    param: btoa(JSON.stringify({
                        perid: data.perid,
                        Project_type: data.Project_type,
                        type: data.type,
                    }))
                }).then((res) => {
                    if(res.data){
                        commit('SET_EXPORT_LIST_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(resErr => {
                    if (resErr) {
                        resolve(false)
                    }
                })
            })
        },
        initAllProjForRegister({ commit }, data) {
            return new Promise((resolve, reject) => {
                axios.post(ConstData.ConnectionHost + '/HrTrain2/serviceAPI2.php', {
                    queryName: 'listAllProjectForRegister',
                    param: btoa(JSON.stringify({
                        perid: data.perid,
                        Project_type: data.Project_type,
                        type: data.type,
                    }))
                }).then((res) => {
                    if(res.data){
                        commit('SET_PROJ_REG_LIST_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(resErr => {
                    if (resErr) {
                        resolve(false)
                    }
                })
            })
        },
        initPlanProjectDataForApprove({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_view/active/1/?sort=project_id_Desc&limit=99999').then((res) => {
                    if(res.data){
                        commit('SET_PLAN_PROJECT_DATA_APPROVE', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initAllPlanProjectDataForApprove({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_view/active/1/?sort=project_id_Desc&limit=99999').then((res) => {
                    if(res.data){
                        commit('SET_PLAN_PROJECT_DATA_APPROVE', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initPlanProjectDataForHistory({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_view/IsPlanStatus/2/active/1/?limit=99999').then((res) => {
                    if(res.data){
                        commit('SET_PLAN_PROJECT_DATA_HISTORY', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initComitteeData({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_committee/active/1').then((res) => {
                    if(res.data){
                        commit('SET_COMMITTEEDATA_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initComitteeType({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/C_committee_view').then((res) => {
                    if(res.data){
                        commit('SET_COMMITTEEDATA_TYPE', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initProvincesData({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/provinces_ms/?limit=100').then((res) => {
                    if(res.data){
                        commit('SET_PROVINCES_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initCountryData({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/tbl_country/?limit=500').then((res) => {
                    if(res.data){
                        commit('SET_COUNTRY_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initExpenseData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/C_cost_project/cost_type/' + data.cost_type + '/active/1/?limit=500').then((res) => {
                    if(res.data){
                        commit('SET_EXPENSE_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initProjectExpenseData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_expense/Project_code/' + data.Project_code + '/active/1/?sort=id_Asc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PEOJECTEXPENSE_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initCompetencyData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_competency_view/Project_code/' + data.Project_code + '/active/1').then((res) => {
                    if(res.data){
                        commit('SET_COMPETENCY_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initProjectGroupData({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/C_project_group/active/1').then((res) => {
                    if(res.data){
                        commit('SET_PROJECTGROUP_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initLecturerData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Lecturer/Ref_id/' + data.Ref_id + '/perid/' + data.perid + '/active/1/?sort=id_Asc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_LECTURER_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initTemplateData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/TemplateProject/Project_code/' + data.Project_code + '/active/1').then((res) => {
                    if(res.data){
                        commit('SET_TEMPLATEPROJECT_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initPersonFileData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Person_files/Project_code/' + data.Project_code + '/active/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PERSONFILE_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initProjectFileData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_files/Type/0/Project_code/' + data.Project_code + '/active/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_PROJECTFILE_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initApproveFormData({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Approve_Form/Project_code/' + data.Project_code + '/active/1/?sort=id_Desc&limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_APPROVE_FORM', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getUrlRegisterBycordi({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_url/' + data.url + '/active/1/?sort=id_Desc&limit=1').then((res) => {
                    if(res.data){
                        commit('SET_URLREGISTER', res.data)
                        resolve(true)
                    } else {
                        commit('SET_URLREGISTER', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_URLREGISTER', [])
                        reject(true)
                    }
                })
            })
        },
        initDateForPlan({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Date_plan/active/1/?sort=date_start_Asc').then((res) => {
                    if(res.data){
                        commit('SET_DATEFORPLAN', res.data)
                        resolve(true)
                    } else {
                        commit('SET_DATEFORPLAN', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_DATEFORPLAN', [])
                        reject(true)
                    }
                })
            })
        },
        initLogProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Log_D_project/Project_code/' + data.Project_code +'/active/1/?sort=id_Desc').then((res) => {
                    if(res.data){
                        commit('SET_LOG_PROJECT', res.data)
                        resolve(true)
                    } else {
                        commit('SET_LOG_PROJECT', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_LOG_PROJECT', [])
                        reject(true)
                    }
                })
            })
        },
        initLogCompetencyProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Log_D_project_competency_view/Ref_id/' + data.Ref_id +'/?sort=id_Desc').then((res) => {
                    if(res.data){
                        commit('SET_LOG_COMPETENCY', res.data)
                        resolve(true)
                    } else {
                        commit('SET_LOG_COMPETENCY', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_LOG_COMPETENCY', [])
                        reject(true)
                    }
                })
            })
        },
        initLogFileProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Log_D_project_files/Ref_id/' + data.Ref_id +'/?sort=id_Desc').then((res) => {
                    if(res.data){
                        commit('SET_LOG_PROJECT_FILE', res.data)
                        resolve(true)
                    } else {
                        commit('SET_LOG_PROJECT_FILE', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_LOG_PROJECT_FILE', [])
                        reject(true)
                    }
                })
            })
        },
        initTempProjectLimit({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Temp_D_project_view/Req_status/0/Project_code/' + data.Project_code +'/active/1/?sort=id_Desc&limit=1').then((res) => {
                    if(res.data){
                        commit('SET_TEMP_PROJECT', res.data)
                        resolve(true)
                    } else {
                        commit('SET_TEMP_PROJECT', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_TEMP_PROJECT', [])
                        reject(true)
                    }
                })
            })
        },
        initTempProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Temp_D_project_view/Project_code/' + data.Project_code +'/active/1/?sort=id_Desc').then((res) => {
                    if(res.data){
                        commit('SET_TEMP_PROJECT', res.data)
                        resolve(true)
                    } else {
                        commit('SET_TEMP_PROJECT', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_TEMP_PROJECT', [])
                        reject(true)
                    }
                })
            })
        },
        initAllTempProject({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Temp_D_project_view/active/1/?sort=id_Desc').then((res) => {
                    if(res.data){
                        commit('SET_TEMP_PROJECT', res.data)
                        resolve(true)
                    } else {
                        commit('SET_TEMP_PROJECT', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_TEMP_PROJECT', [])
                        reject(true)
                    }
                })
            })
        },
        initTempCompetencyProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Temp_D_project_competency_view/Ref_id/' + data.Ref_id +'/?sort=id_Desc').then((res) => {
                    if(res.data){
                        commit('SET_TEMP_COMPETENCY', res.data)
                        resolve(true)
                    } else {
                        commit('SET_TEMP_COMPETENCY', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_TEMP_COMPETENCY', [])
                        reject(true)
                    }
                })
            })
        },
        initTempFileProject({ commit }, data) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Temp_D_project_files/Ref_id/' + data.Ref_id +'/?sort=id_Desc').then((res) => {
                    if(res.data){
                        commit('SET_TEMP_PROJECT_FILE', res.data)
                        resolve(true)
                    } else {
                        commit('SET_TEMP_PROJECT_FILE', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_TEMP_PROJECT_FILE', [])
                        reject(true)
                    }
                })
            })
        },
        initStatusProject({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/D_project_status/active/1/?sort=sort_ASC').then((res) => {
                    if(res.data){
                        commit('SET_STATUS_PROJECT', res.data)
                        resolve(true)
                    } else {
                        commit('SET_STATUS_PROJECT', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_STATUS_PROJECT', [])
                        reject(true)
                    }
                })
            })
        },
        initTempStatusProject({ commit }) {
            return new Promise((resolve, reject) => {
                ServiceProject.initData('/hrtrain/Temp_D_project_status').then((res) => {
                    if(res.data){
                        commit('SET_TEMP_STATUS_PROJECT', res.data)
                        resolve(true)
                    } else {
                        commit('SET_TEMP_STATUS_PROJECT', [])
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_TEMP_STATUS_PROJECT', [])
                        reject(true)
                    }
                })
            })
        },
        initFileReportTravel({ commit }, data) {
            return new Promise((resolve) => {
                ServiceProject.initData('/hrtrain/travel_and_training_form_view/Project_code/' + data.Project_code).then((res) => {
                    if(res.data){
                        commit('SET_TRAVEL_FORM', res.data)
                        resolve(true)
                    } else {
                        commit('SET_TRAVEL_FORM', [])
                        resolve(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_TRAVEL_FORM', [])
                        resolve(true)
                    }
                })
            })
        },
        initJoinTravel({ commit }, data) {
            return new Promise((resolve) => {
                ServiceProject.initData('/hrtrain/join_travel_view/Project_code/' + data.Project_code + "/active/1/?limit=9999").then((res) => {
                    if(res.data){
                        commit('SET_JOIN_TRAVEL', res.data)
                        resolve(true)
                    } else {
                        commit('SET_JOIN_TRAVEL', [])
                        resolve(true)
                    }
                }).catch(res => {
                    if (res) {
                        commit('SET_JOIN_TRAVEL', [])
                        resolve(true)
                    }
                })
            })
        },
        changeTypeProject({ commit }, data) {
            commit('SET_TYPE_PROJECT', data.type)
            commit('SET_APPROVE_BY_HR', data.hr)
        },
        changeHeaderPage({ commit }, data) {
            commit('SET_HEADER_PAGE', data.page)
        },
        changePlanProjectPage({ commit }, data) {
            commit('SET_PLAN_PROJECT_PAGE', data.page)
        },
        changeManageProjectPage({ commit }, data) {
            commit('SET_MANAGE_PROJECT_PAGE', data.page)
        },
        changeJoinProjectPage({ commit }, data) {
            commit('SET_JOIN_PROJECT_PAGE', data.page)
        },
    },
    getters: {
        getProjectRound(state) {
            return state.Project_RoundData
        },
        getGroupQuota(state) {
            return state.Group_Quota
        },
        getGroupQuota_Ms(state) {
            return state.Group_Quota_Ms
        },
        getProjectPlace_Ms(state) {
            return state.Project_Place_Ms
        },
        getTrain_Ms(state) {
            return state.Train_Ms
        },
        getCommittee_Ms(state) {
            return state.Committee_Ms
        },
        getOrganizer_Ms(state) {
            return state.Organizer_Ms
        },
        getPresentation_Status_Ms(state) {
            return state.Presentation_Status_Ms
        },
        getCourseWorkExp(state) {
            return state.CourseWorkExp
        },
        getWorkExp(state) {
            return state.WorkExpData
        },
        getProjectPlanApprove(state) {
            return state.PlanProjectDataApprove
        },
        getProjectPlanHistory(state) {
            return state.PlanProjectDataHistory
        },
        getProjectPlan(state) {
            return state.ProjectPlanData
        },
        getGroupExamProject(state) {
            return state.GroupExamProject
        },
        getProjectReg(state) {
            return state.ProjectRegData
        },
        getQuotaDepart(state) {
            return state.QuotaDepart
        },
        getTrainFiles(state) {
            return state.Train_Files
        },
        getAward(state) {
            return state.AwardData
        },
        getJournal(state) {
            return state.JournalData
        },
        getJoinProject(state) {
            return state.joinProjectData
        },
        getRegisterProject(state) {
            return state.registerProjectData
        },
        getRequestProject(state) {
            return state.requestProjectData
        },
        getHistoryExpert(state) {
            return state.historyExpert
        },
        getBoardOption(state) {
            return state.boardOption
        },
        getTrainHisProject(state){
            return state.TrainHistory
        },
        getCommitHisProject(state){
            return state.CommitHistory
        },
        getPresentHisProject(state){
            return state.PresentHistory
        },
        getTextForPrint(state){
            return state.TextForPrint
        },
        getCountry(state){
            return state.CountryData
        },
        getApproveProject(state){
            return state.Approve_Project
        },
        getProjectForApprove(state){
            return state.ProjectDataApprove
        },
        getProject(state){
            return state.ProjectData
        },
        getProjectRecord(state){
            return state.ProjectRecord
        },
        getComittee(state){
            return state.CommitteeData
        },
        getComitteeType(state){
            return state.CommitteeType
        },
        getProvince(state){
            return state.ProvincesData
        },
        getExpense(state){
            return state.ExpenseData
        },
        getProjectExpense(state){
            return state.ProjectExpenseData
        },
        getCompetency(state){
            return state.CompetencyData
        },
        getProjectGroup(state){
            return state.ProjectGroupData
        },
        getLecturer(state){
            return state.LecturerData
        },
        getTemplateProject(state){
            return state.TemplateProjectData
        },
        getProjectFiles(state){
            return state.ProjectFiles
        },
        getPersonFiles(state){
            return state.PersonFiles
        },
        getApproveForm(state){
            return state.ApproveForm
        },
        getUrl(state){
            return state.UrlRegister
        },
        getDateForPlan(state){
            return state.DateForPlan
        },
        getLogProject(state){
            return state.Log_project
        },
        getLogCompetency(state){
            return state.Log_competency
        },
        getLogFiles(state){
            return state.Log_project_file
        },
        getTempProject(state){
            return state.Temp_project
        },
        getTempCompetency(state){
            return state.Temp_competency
        },
        getTempFiles(state){
            return state.Temp_project_file
        },
        getTypeProject(state){
            return state.TypeProject
        },
        getHeaderPage(state){
            return state.HeaderPage
        },
        getPlanProjectPage(state){
            return state.PlanProjectPage
        },
        getManageProjectPage(state){
            return state.ManageProjectPage
        },
        getJoinProjectPage(state){
            return state.JoinProjectPage
        },
        getStatusProject(state){
            return state.StatusProject
        },
        getTempStatusProject(state){
            return state.TempStatusProject
        },
        getTravelForm(state){
            return state.TravelFrom
        },
        getJoinTravel(state){
            return state.JoinTravel
        },
        getProjectExport(state){
            return state.ProjectExportData
        },
        getAllProjForRegister(state){
            return state.ProjectRegListData
        },
    }
}

export default projectModule
