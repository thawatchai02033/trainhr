import Vue from 'vue'
import Vuex from 'vuex'
import LevelModule from './Level_module'
import DepartModule from './Depart_module'
import PositionsModule from './Positions_module'
import CourseModule from './Course_module'
import StaffModule from './Staff_module'
import ProjectModule from './Project_module'
import CompetencyModule from './Competency_module'
import permissionModule from './Permission_module'
import MeetingModule from './Meeting_module'
import AppointmentModule from './Appointment_module'
import PageModule from "@/store/Page_module";
// Make vue aware of Vuex
Vue.use(Vuex)

const modules = {
  Level: LevelModule,
  Depart: DepartModule,
  Positions: PositionsModule,
  Course: CourseModule,
  Staff: StaffModule,
  Project: ProjectModule,
  Competency: CompetencyModule,
  Permission: permissionModule,
  Meeting: MeetingModule,
  Appointment: AppointmentModule,
  Page: PageModule
}

// Combine the initial state and the mutations to create a Vuex store.
// This store can be linked to our app.
export default new Vuex.Store({
  modules
})
