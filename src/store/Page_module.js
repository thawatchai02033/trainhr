const pageModule = {
    namespaced: true,
    state: {
        appColor: 'primary',
        appHeaderName: 'โครงการจัดอบรมและการเดินทางไปปฏิบัติงาน',
        appHeaderName1: 'จัดโครงการ/ค่าใช้จ่ายอื่น',
        appHeaderName2: 'อบรม / เดินทางไปปฏิบัติงาน',
        appDialogLoading: true
    },
    mutations: {
        SET_APPCOLOR(state, data) {
            state.appColor = data
        },
        SET_HEADERNAME(state, data) {
            state.appHeaderName = data
        },
        SET_DIALOG_LOADING(state, data) {
            state.appDialogLoading = data
        },
    },
    actions: {
        changeColor({ commit }, data) {
            commit('SET_APPCOLOR', data.color)
        },
        changeHeaderName({ commit }, data) {
            commit('SET_HEADERNAME', data.headerName)
        },
        changeDialogLoading({ commit }, data) {
            commit('SET_DIALOG_LOADING', data.loading)
        },
    },
    getters: {
        getColor(state){
            return state.appColor
        },
        getHeaderName(state){
            return state.appHeaderName
        },
        getDialogLoading(state){
            return state.appDialogLoading
        },
    }
}

export default pageModule
