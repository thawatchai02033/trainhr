import {PositionService} from '../resource'

const positionModule = {
    namespaced: true,
    state: {
        positionData: [],
        posi_ManagerialData: [],
    },
    mutations: {
        SET_POSITION_DATA(state, data) {
            state.positionData = []
            if (data.length > 0) {
                state.positionData = data
            } else {
                state.positionData.push(data)
            }
        },
        SET_MANAGERIAL_DATA(state, data) {
            state.posi_ManagerialData = []
            if (data.length > 0) {
                state.posi_ManagerialData = data
            } else {
                state.posi_ManagerialData.push(data)
            }
        }
    },
    actions: {
        initPositionData({ commit }) {
            return new Promise((resolve, reject) => {
                PositionService.initData('/hrtrain/position_ms_view/active/1/?limit=2000').then((res) => {
                    if(res.data){
                        commit('SET_POSITION_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
        initPosi_ManagerialData({ commit }) {
            return new Promise((resolve, reject) => {
                PositionService.initData('/hrtrain/supervisor_type_ms/?limit=2000').then((res) => {
                    if(res.data){
                        commit('SET_MANAGERIAL_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
    },
    getters: {
        getPosition(state){
            return state.positionData
        },
        getPositionManagerial(state){
            return state.posi_ManagerialData
        },
    }
}

export default positionModule
