import {CompetencyService} from '@/resource'

const CompetencyModule = {
    namespaced: true,
    state: {
        CompetencyData: [],
    },
    mutations: {
        SET_COMPETENCY_DATA(state, data) {
            state.CompetencyData = []
            if (data.length > 0) {
                state.CompetencyData = data
            } else {
                state.CompetencyData.push(data)
            }
        },
    },
    actions: {
        initCompetencyData({ commit }) {
            return new Promise((resolve, reject) => {
                CompetencyService.initData('/hrtrain/C_perseondelv_view/?limit=9999').then((res) => {
                    if(res.data){
                        commit('SET_COMPETENCY_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                })
            })
        },
    },
    getters: {
        getCompetency(state){
            return state.CompetencyData
        },
    }
}

export default CompetencyModule