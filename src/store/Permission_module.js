import {PermissionService} from '../resource'

const permissionModule = {
    namespaced: true,
    state: {
        PermissionDepUserData: [],
        PermissionDepData: [],
        PermissionOptData: [],
        PermissionOptUserData: [],
        PermissionAllUserData: [],
        PermissionAllData: [],
        PermissionUserData: [],
        PermissionData: [],
        PermissionApproveData: [],
        PermissionUserApproveData: [],
        Permission_Ms: [],
        Permission_Dep_User: [],
        Permission_Dep_User_All: [],
        Permission_Dep: [],
        Approve_Ms: [],
        ApproveData: [],
        MenuPermiss: [],
        IgnoreMenuPermiss: [],
        SubMenuPermiss: [],
        IgnoreSubMenuPermiss: [],
    },
    mutations: {
        SET_APPROVE_MS(state, data) {
            state.Approve_Ms = []
            if (data.length > 0) {
                state.Approve_Ms = data
            } else {
                state.Approve_Ms.push(data)
            }
        },
        SET_APPROVE_DATA(state, data) {
            state.ApproveData = []
            if (data.length > 0) {
                state.ApproveData = data
            } else {
                state.ApproveData.push(data)
            }
        },
        SET_PERMISSIONDEPALLUSER_DATA(state, data) {
            state.PermissionDepUserData = []
            if (data.length > 0) {
                state.PermissionDepUserData = data
            } else {
                state.PermissionDepUserData.push(data)
            }
        },
        SET_PERMISSIONALLUSER_DATA(state, data) {
            state.PermissionAllUserData = []
            if (data.length > 0) {
                state.PermissionAllUserData = data
            } else {
                state.PermissionAllUserData.push(data)
            }
        },
        SET_PERMISSIONDEPALL_DATA(state, data) {
            state.PermissionDepData = []
            if (data.length > 0) {
                state.PermissionDepData = data
            } else {
                state.PermissionDepData.push(data)
            }
        },
        SET_PERMISSIONOPTION_DATA(state, data) {
            state.PermissionOptData = []
            if (data.length > 0) {
                state.PermissionOptData = data
            } else {
                state.PermissionOptData.push(data)
            }
        },
        SET_PERMISSIONOPTION_USER_DATA(state, data) {
            state.PermissionOptUserData = []
            if (data.length > 0) {
                state.PermissionOptUserData = data
            } else {
                state.PermissionOptUserData.push(data)
            }
        },
        SET_PERMISSIONALL_DATA(state, data) {
            state.PermissionAllData = []
            if (data.length > 0) {
                state.PermissionAllData = data
            } else {
                state.PermissionAllData.push(data)
            }
        },
        SET_PERMISSION_USER_DATA(state, data) {
            state.PermissionUserData = []
            if (data.length > 0) {
                state.PermissionUserData = data
            } else {
                state.PermissionUserData.push(data)
            }
        },
        SET_PERMISSION_DATA(state, data) {
            state.PermissionData = []
            if (data.length > 0) {
                state.PermissionData = data
            } else {
                state.PermissionData.push(data)
            }
        },
        SET_PERMISSION_APPROVE_DATA(state, data) {
            state.PermissionApproveData = []
            if (data.length > 0) {
                state.PermissionApproveData = data
            } else {
                state.PermissionApproveData.push(data)
            }
        },
        SET_PERMISSION_USER_APPROVE_DATA(state, data) {
            state.PermissionUserApproveData = []
            if (data.length > 0) {
                state.PermissionUserApproveData = data
            } else {
                state.PermissionUserApproveData.push(data)
            }
        },
        SET_PERMISSION_MS(state, data) {
            state.Permission_Ms = []
            if (data.length > 0) {
                state.Permission_Ms = data
            } else {
                state.Permission_Ms.push(data)
            }
        },
        SET_PERMISSION_DEP_USER(state, data) {
            state.Permission_Dep_User = []
            if (data.length > 0) {
                state.Permission_Dep_User = data
            } else {
                state.Permission_Dep_User.push(data)
            }
        },
        SET_PERMISSION_DEP_USER_ALL(state, data) {
            state.Permission_Dep_User_All = []
            if (data.length > 0) {
                state.Permission_Dep_User_All = data
            } else {
                state.Permission_Dep_User_All.push(data)
            }
        },
        SET_PERMISSION_DEP(state, data) {
            state.Permission_Dep = []
            if (data.length > 0) {
                state.Permission_Dep = data
            } else {
                state.Permission_Dep.push(data)
            }
        },
        SET_MENUPERMISS(state, data) {
            state.MenuPermiss = []
            if (data.length > 0) {
                state.MenuPermiss = data
            } else {
                state.MenuPermiss.push(data)
            }
        },
        SET_SUBMENUPERMISS(state, data) {
            state.SubMenuPermiss = []
            if (data.length > 0) {
                state.SubMenuPermiss = data
            } else {
                state.SubMenuPermiss.push(data)
            }
        },
        SET_IGNOREMENUPERMISS(state, data) {
            state.IgnoreMenuPermiss = []
            if (data.length > 0) {
                state.IgnoreMenuPermiss = data
            } else {
                state.IgnoreMenuPermiss.push(data)
            }
        },
        SET_IGNORESUBMENUPERMISS(state, data) {
            state.IgnoreSubMenuPermiss = []
            if (data.length > 0) {
                state.IgnoreSubMenuPermiss = data
            } else {
                state.IgnoreSubMenuPermiss.push(data)
            }
        }
    },
    actions: {
        initApproveMsData({commit}, data) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Approve_Ms/dep_id/' + data.dep_id + '/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_APPROVE_MS', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initApproveData({commit}) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Approve_view/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_APPROVE_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initPermissionMsData({commit}) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_ms/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSION_MS', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initPermissionDepUserData({commit}, data) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_Depart_User_view/perid/' + data.perid + '/active/1/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSIONDEPALLUSER_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initPermissionUserData({commit}, data) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_User_view/create_by/' + data.perid + '/active/1/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSIONALLUSER_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initPermissionDepUserAllData({commit}) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_Depart_User_view/active/1/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSION_DEP_USER_ALL', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getPermissionUserData({commit}, data) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_User/perid/' + data.perid + '/active/1/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSION_USER_DATA', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getPermissionDepUserData({commit}, data) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_Depart_User/perid/' + data.perid + '/active/1/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSION_DEP_USER', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initPermissionDepData({commit}) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_Depart_view/active/1/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSIONDEPALL_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initPermissionOptionData({commit}, data) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_Option/perid/' + data.perid + '/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSIONOPTION_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initPermissionOptionUserData({commit}, data) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_Option_User/perid/' + data.perid + '/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSIONOPTION_USER_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        initPermissionData({commit}) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_view/active/1/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSIONALL_DATA', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getPermissionData({commit}, data) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission/perid/' + data.perid + '/active/1/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSION_DATA', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getPermissionDepData({commit}, data) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_Depart/perid/' + data.perid + '/active/1/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_PERMISSION_DEP', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getMenuPermissData({commit}) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/PageStatus/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_MENUPERMISS', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getSubMenuPermissData({commit}) {
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/PageStatus_sub/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_SUBMENUPERMISS', res.data)
                        resolve(true)
                    } else {
                        reject(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getIgnoreMenuPermissData({commit}, data) {
            return new Promise((resolve) => {
                PermissionService.initData('/hrtrain/PageStatus_ignore/perid/' + data.perid + '/PageStatus_page_id/' + data.page_id + '/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_IGNOREMENUPERMISS', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        resolve(false)
                    }
                })
            })
        },
        getIgnoreSubMenuPermissData({commit}, data) {
            return new Promise((resolve) => {
                PermissionService.initData('/hrtrain/PageStatus_sub_ignore/perid/' + data.perid + '/PageStatus_sub_page_sub_id/' + data.page_sub_id + '/PageStatus_page_id/' + data.page_id + '/?limit=9999').then((res) => {
                    if (res.data) {
                        commit('SET_IGNORESUBMENUPERMISS', res.data)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }).catch(res => {
                    if (res) {
                        resolve(false)
                    }
                })
            })
        },
        getPermissionByApprove({commit}, data) {
            var arrayApprove = []
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_Depart/dep_id/' + data.dep_id + '/active/1/?limit=9999').then(async (resPermissDep) => {
                    if (resPermissDep.data) {
                        if (resPermissDep.data.length > 0) {
                            await resPermissDep.data.map(async (item, idx) => {
                                await PermissionService.initData('/hrtrain/Permission/perid/' + item.perid + '/permission_id/' + data.permission_id + '/active/1/?limit=9999').then(async (res) => {
                                    if (res.data) {
                                        if (res.data.length > 0) {
                                            arrayApprove.concat(res.data)
                                        } else {
                                            arrayApprove.push(res.data)
                                        }
                                    }
                                    if (idx == resPermissDep.data.length - 1) {
                                        commit('SET_PERMISSION_APPROVE_DATA', arrayApprove)
                                        resolve(true)
                                    }
                                })
                            })
                        } else {
                            PermissionService.initData('/hrtrain/Permission/perid/' + resPermissDep.data.perid + '/permission_id/' + data.permission_id + '/active/1/?limit=9999').then((res) => {
                                if (res.data) {
                                    commit('SET_PERMISSION_APPROVE_DATA', res.data)
                                    resolve(true)
                                } else {
                                    resolve(true)
                                }
                            }).catch(res => {
                                if (res) {
                                    reject(true)
                                }
                            })
                        }
                    } else {
                        resolve(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
        getPermissionUserByApprove({commit}, data) {
            var arrayApprove = []
            return new Promise((resolve, reject) => {
                PermissionService.initData('/hrtrain/Permission_Depart_User/dep_id/' + data.dep_id + '/active/1/?limit=9999').then(async (resPermissDep) => {
                    if (resPermissDep.data) {
                        if (resPermissDep.data.length > 0) {
                            await resPermissDep.data.map(async (item, idx) => {
                                await PermissionService.initData('/hrtrain/Permission_User/perid/' + item.perid + '/permission_id/' + data.permission_id + '/active/1/?limit=9999').then(async (res) => {
                                    if (res.data) {
                                        if (res.data.length > 0) {
                                            arrayApprove.concat(res.data)
                                        } else {
                                            arrayApprove.push(res.data)
                                        }
                                    }
                                    if (idx == resPermissDep.data.length - 1) {
                                        commit('SET_PERMISSION_USER_APPROVE_DATA', arrayApprove)
                                        resolve(true)
                                    }
                                })
                            })
                        } else {
                            PermissionService.initData('/hrtrain/Permission_User/perid/' + resPermissDep.data.perid + '/permission_id/' + data.permission_id + '/active/1/?limit=9999').then((res) => {
                                if (res.data) {
                                    commit('SET_PERMISSION_USER_APPROVE_DATA', res.data)
                                    resolve(true)
                                } else {
                                    resolve(true)
                                }
                            }).catch(res => {
                                if (res) {
                                    reject(true)
                                }
                            })
                        }
                    } else {
                        resolve(true)
                    }
                }).catch(res => {
                    if (res) {
                        reject(true)
                    }
                })
            })
        },
    },
    getters: {
        getApproveMs(state) {
            return state.Approve_Ms
        },
        getApprove(state) {
            return state.ApproveData
        },
        getAllUserPermission(state) {
            return state.PermissionAllUserData
        },
        getAllDepUserPermission(state) {
            return state.Permission_Dep_User_All
        },
        getDepUserPermission(state) {
            return state.PermissionDepUserData
        },
        getOptPermission(state) {
            return state.PermissionOptData
        },
        getOptUserPermission(state) {
            return state.PermissionOptUserData
        },
        getAllDepPermission(state) {
            return state.PermissionDepData
        },
        getAllPermission(state) {
            return state.PermissionAllData
        },
        getPermissionUser(state) {
            return state.PermissionUserData
        },
        getPermission(state) {
            return state.PermissionData
        },
        getPermissionMs(state) {
            return state.Permission_Ms
        },
        getPermissionDepUser(state) {
            return state.Permission_Dep_User
        },
        getPermissionDep(state) {
            return state.Permission_Dep
        },
        getMenuPermiss(state) {
            return state.MenuPermiss
        },
        getIgnoreMenuPermiss(state) {
            return state.IgnoreMenuPermiss
        },
        getSubMenuPermiss(state) {
            return state.SubMenuPermiss
        },
        getIgnoreSubMenuPermiss(state) {
            return state.IgnoreSubMenuPermiss
        },
        getPermissionApprove(state) {
            return state.PermissionApproveData
        },
        getPermissionUserApprove(state) {
            return state.PermissionUserApproveData
        }
    }
}

export default permissionModule
