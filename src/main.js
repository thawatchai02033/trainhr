import Vue from 'vue'
import App from './App.vue'
import 'vuetify/dist/vuetify.min.css'
import vuetify from './plugins/vuetify';
import Router from './router/index'
import VueSession from 'vue-session'
import VueSweetalert2 from 'vue-sweetalert2';
import filter from './utils/filter'
import store from './store'
import draggable from 'vuedraggable'
import CKEditor from 'ckeditor4-vue';
import VueHtmlToPaper from 'vue-html-to-paper';
import VueHtml2pdf from 'vue-html2pdf'
import JsonExcel from "vue-json-excel";
import VueCurrencyFilter from 'vue-currency-filter'


// import VueSocketIO from 'vue-socket.io'
//
// import SocketIO from 'socket.io-client'

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
import VueCookies from 'vue-cookies'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import UUID from "vue-uuid";
import axios from "axios";
import ConstData from "@/const/Const";
import queryString from "query-string";

//Import Froala Editor
import 'froala-editor/js/plugins.pkgd.min.js';
import 'froala-editor/js/plugins/word_paste.min';
import 'froala-editor/js/plugins/font_family.min';
import 'froala-editor/js/plugins/line_height.min';
import 'froala-editor/js/plugins/font_size.min';
//Import third party plugins
import 'froala-editor/js/third_party/embedly.min';
import 'froala-editor/js/third_party/font_awesome.min';
import 'froala-editor/js/third_party/spell_checker.min';
import 'froala-editor/js/third_party/image_tui.min';
// Import Froala Editor css files.
import 'froala-editor/css/froala_editor.pkgd.min.css';
import VueFroala from 'vue-froala-wysiwyg'


const options = {
    name: '_blank',
    specs: [
        'fullscreen=yes',
        'titlebar=yes',
        'scrollbars=yes'
    ],
    styles: [
        'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900',
        'https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css'
    ]
}

Vue.mixin({
    methods: {
        SendMail: function(data) {
            axios.post(ConstData.ConnectionHost + '/sendMail/SendMailData.php',  queryString.stringify({
                perid_admin: data.peridToReceive, // ผู้รับ
                perid: data.peridToSend, // ผู้ส่ง
                headerName: data.headerName,
                header: data.header,
                body: data.body,
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            })
        },
        SendGMail: function(data) {
            axios.post(ConstData.ConnectionHost + '/sendMail/SendGMailData.php',  queryString.stringify({
                headerName: data.headerName,
                header: data.header,
                body: data.body,
                email: data.email
            }), {
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            })
        }
    }
});
Vue.use(VueFroala)
Vue.use(VueHtmlToPaper, options);

Vue.use(VueSession)
Vue.use(VueSweetalert2);
Vue.use(VueCookies)
Vue.use(VueCookies)
Vue.use(CKEditor)
Vue.use(VueHtml2pdf)
Vue.use(UUID);
Vue.use(VueCurrencyFilter,
    {
        thousandsSeparator: ',',
        symbolPosition: 'front',
        symbolSpacing: true,
        avoidEmptyDecimals: '',
    })

// Vue.use(new VueSocketIO({
//         debug: true,
//         connection: SocketIO('http://88dc-61-19-201-8.ngrok.io', { path: '' }), //options object is Optional http://localhost:9898
//         vuex: {
//             store,
//             actionPrefix: "SOCKET_",
//             mutationPrefix: "SOCKET_"
//         }
//     })
// );
//
// Vue.use(new VueSocketIO({
//         debug: true,
//         connection: SocketIO('http://70f8-61-19-201-8.ngrok.io', { path: '' }), //options object is Optional http://localhost:9898
//         vuex: {
//             store,
//             actionPrefix: "SOCKET_",
//             mutationPrefix: "SOCKET_"
//         }
//     })
// );

// Vue.use(new VueSocketIO({
//     debug: true,
//     connection: 'http://localhost:9898',
//     vuex: {
//         store,
//         actionPrefix: 'SOCKET_',
//         mutationPrefix: 'SOCKET_'
//     },
//     options: { path: "" } //Optional options
// }))

Vue.config.productionTip = false

Vue.component("downloadExcel", JsonExcel);

Vue.filter('thaiBaht', function (amount) {
    if (amount != null && amount != '') {
        return amount.toLocaleString('th-TH', {
            style: 'decimal',
            minimumFractionDigits: 2
            // minimumFractionDigits: 0,
            // maximumFractionDigits: 0
            // }) + ' บาท';
        });
    } else {
        return '0'
    }
})
Vue.filter('quot', function (value) {
    return value.replaceAll('&quot;', '"')

})

Vue.filter('DateToThai', function (value) {
    var DateTrans = "";
    if (value != null && value != '0000-00-00') {
        var Day = value.split('-')[2];
        var Month = value.split('-')[1];
        var Year = value.split('-')[0];
        var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
            'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
        ];
        DateTrans = Day.split(' ')[0] + " " + monthNames[parseInt(Month) - 1] + " " +
            (parseInt(Year) + 543)
    }
    return (
        DateTrans
    );
})

Vue.filter('dateToThai2', (value) => {
    var DateTrans = "";
    if (value != undefined) {
        var date = value.split(' ')[0]
        var time = value.split(' ')[1]
        var Day = date.split('-')[2];
        var Month = date.split('-')[1];
        var Year = date.split('-')[0];
        var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
            'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
        ];
        DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
            (parseInt(Year) + 543) + ' ' + time + ' น.'
    }
    return (
        DateTrans
    );
})

Vue.filter('YearToThai', function (value) {
    return (
        parseInt(value) + 543
    );
})

Vue.filter('truncateText', function (value) {
    if (value.length > 20) {
        return value.substring(0, 20) + "...";
    } else {
        return value;
    }
})

new Vue({
    draggable,
    router: Router,
    vuetify,
    filter,
    store,
    render: h => h(App)
}).$mount('#app')
