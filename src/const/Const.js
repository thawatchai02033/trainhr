// const ConnectionHost = "http://localhost:8080"
// // const ConnectionHost = "https://medhr.medicine.psu.ac.th"

const ConnectionHost = process.env.NODE_ENV === 'development'? "/MedHr" : "https://medhr.medicine.psu.ac.th"
const ConnectionHostHrTrain = process.env.NODE_ENV === 'development'? 'https://medhr.medicine.psu.ac.th/HrTrain2/#' : "https://medhr.medicine.psu.ac.th/HrTrain2/#"
const ConnectionHostHrTrain2 = process.env.NODE_ENV === 'development'? 'https://medhr.medicine.psu.ac.th/HrTrain2/#/' : "https://medhr.medicine.psu.ac.th/HrTrain2/#/"
const PreGenerateLink = process.env.NODE_ENV === 'development'? 'http://localhost:8080/#/JoinMeeting/' : 'https://medhr.medicine.psu.ac.th/HrTrain2/#/JoinMeeting/'
const ProjectGenerateLink = process.env.NODE_ENV === 'development'? 'http://localhost:8080/#/RequestToJoinProject/': 'https://medhr.medicine.psu.ac.th/HrTrain2/#/RequestToJoinProject/'
const ConnectionMedNet = process.env.NODE_ENV === 'development'? "/MedNet" : "https://mednet.psu.ac.th"
const ConnectionLine = process.env.NODE_ENV === 'development'? "/Line" : "https://notify-api.line.me"
const defaultHeaderApp = 'โครงการจัดอบรมและการเดินทางไปปฏิบัติงาน'
const defaultHeaderApp1 = 'จัดโครงการ / ค่าใช้จ่ายอื่น'
const defaultHeaderApp2 = 'อบรม / เดินทางไปปฏิบัติงาน'
const defaultColorApp = '#1976d2'
// const ConnectionMedNet = "https://mednet.psu.ac.th"
// const ConnectionHost = "https://medhr.medicine.psu.ac.th"
// const ConnectionHostHrTrain = "https://medhr.medicine.psu.ac.th/HrTrain2/#"
// const ConnectionHostHrTrain2 = "https://medhr.medicine.psu.ac.th/HrTrain2/#/"
// const PreGenerateLink = 'https://medhr.medicine.psu.ac.th/HrTrain2/#/JoinMeeting/'
// const ProjectGenerateLink = 'https://medhr.medicine.psu.ac.th/HrTrain2/#/RequestToJoinProject/'
// const ConnectionLine = "https://notify-api.line.me"

export default {
    ConnectionHost: ConnectionHost,
    ConnectionHostHrTrain: ConnectionHostHrTrain,
    ConnectionHostHrTrain2: ConnectionHostHrTrain2,
    PreGenerateLink: PreGenerateLink,
    ProjectGenerateLink: ProjectGenerateLink,
    ConnectionMedNet: ConnectionMedNet,
    ConnectionLine: ConnectionLine,
    defaultHeaderApp: defaultHeaderApp,
    defaultHeaderApp1: defaultHeaderApp1,
    defaultHeaderApp2: defaultHeaderApp2,
    defaultColorApp: defaultColorApp,
}
