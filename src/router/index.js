import Vue from 'vue'
import Router from 'vue-router'
import RegisterProjectOther from '../components/RegisterProject'
import PrintData from '../components/PrintData'
import Train from '../components/Train'
import ApproveProject from '../components/ApproveProject'
import ApproveEditProject from '../components/ApproveEditProject'
import ApproveTravelByDep from '../components/ApproveTravelByDep'
import JoinMeeting from '../components/JoinMeeting'
import RequestToJoinProject from '../components/RequestToJoinProject'
import Home from '../components/Home'
import ManageCourse from '../components/ManageCourse'
import ManageProject from '../components/ManageProject'
import Project from '../components/Project'
import BudgetProject from '../components/BudgetProject'
import Propose_a_plan from '../components/Propose_a_plan'
import JoinProject from '../components/JoinProject'
import SettingCompetency from '../components/SettingCompetency'
import SettingManagerial from '../components/SettingManagerial'
import SettingPermission from '../components/SettingPermission'
import SettingDateForPlan from '../components/SettingDateForPlan'
import SettingExpenses from '../components/SettingExpenses'
import Permission from '../components/Permission'
import Report from '../components/Report'
import ManagePerson from '../components/ManagePerson'
import Item_Code from '../components/Item_Code'
import PrintFormProject from '../components/PrintFormProject'
import Report30per from '../components/NestedManageReport/Report30per.vue'
import AcademicPosition from '../components/NestedManageReport/AcademicPosition.vue'
import ProjectData from '../components/NestedManageProject/Project'
import planProject from '../components/NestedManageProject/planProject'
import InsertProject from '../components/NestedManageProject/InsertProject'
import UpdateProject from '../components/NestedManageProject/UpdateProject'
import SettingData from '../components/NestedManageProject/SettingData'
import PlanProject from '../components/NestedPropose_A_Plan/PlanProject'
import InsertPlanProject from '../components/NestedPropose_A_Plan/InsertPlanProject'
import EditPlanProject from '../components/NestedPropose_A_Plan/EditPlanProject'
import PositionPerCourse from '../components/PositionPerCourse'
import DataCourse from '../components/NestedManageCourse/DataCourse'
import InsertCourse from '../components/NestedManageCourse/InsertCourse'
import UpdateCourse from '../components/NestedManageCourse/UpdateCourse'
import NestedJoinProject from "@/components/NestedJoinProject/JoinProject";
import RegisterProject from "@/components/NestedJoinProject/RegisterProject";
import RequestJoinProject from "../components/NestedJoinProject/RequestJoinProject";
import ExportListData from "../components/NestedJoinProject/ExportListData";
import Meeting from "../components/Meeting";
import ReggisterMeeting from "../components/ReggisterMeeting";
import Project2 from "@/components/Project2";
import MeetingData from '@/components/NestedMeeting/MeetingData'
import ManageMeeting from '@/components/NestedMeeting/ManageMeeting'
import JoinMeetingPerson from '@/components/JoinMeetingPerson'
import AcademicFundsRemaining from '@/components/AcademicFundsRemaining'
import Appointment from "../components/Appointment";
import ManageAppointment from '@/components/NestedAppointment/ManageAppointment'
import InsertAppointment from '@/components/NestedAppointment/InsertAppointment'

Vue.use(Router)

export default new Router({
    routes:
        [
            {
                path: '/RegisterProjectOther',
                name: 'RegisterProjectOther',
                component: RegisterProjectOther,
            },
            {
                path: '/PrintData',
                name: 'PrintData',
                component: PrintData,
            },
            {
                path: '/JoinMeeting/:uuid',
                name: 'JoinMeeting',
                component: JoinMeeting,
                props: true
            },
            {
                path: '/RequestToJoinProject/:uuid/:id',
                name: 'RequestToJoinProject',
                component: RequestToJoinProject,
                props: true
            },
            {
                path: '/',
                name: 'Train',
                component: Train,
                children: [
                    {
                        path: '/',
                        name: 'index',
                        component: Home
                    },
                    {
                        path: '/Project2',
                        name: 'Project2',
                        component: Project2
                    },
                    {
                        path: '/JoinProject',
                        name: 'เข้าร่วมโครงการ',
                        component: JoinProject,
                        children: [
                            {
                                path: 'RequestJoinProject',
                                name: 'RequestJoinProject',
                                component: RequestJoinProject,
                            },
                            {
                                path: 'JoinProject',
                                name: 'JoinProject',
                                component: NestedJoinProject,
                            },
                            {
                                path: 'RegisterProject',
                                name: 'RegisterProject',
                                component: RegisterProject,
                            },
                            {
                                path: 'ExportListData',
                                name: 'ExportListData',
                                component: ExportListData,
                            }
                        ]
                    },
                    {
                        path: '/Project',
                        name: 'ยื่นเสนอโครงการ',
                        component: Project
                    },
                    {
                        path: '/BudgetProject',
                        name: 'BudgetProject',
                        component: BudgetProject
                    },
                    {
                        path: '/ManageCourse',
                        name: 'จัดการข้อมูลหลักสูตร',
                        component: ManageCourse,
                        children: [
                            {
                                path: 'DataCourse',
                                name: 'ข้อมูลหลักสูตร',
                                component: DataCourse,
                            },
                            {
                                path: 'InsertCourse',
                                name: 'เพิ่มข้อมูลหลักสูตร',
                                component: InsertCourse,
                            },
                            {
                                path: 'UpdateCourse',
                                name: 'แก้ไขข้อมูลหลักส฿ตร',
                                component: UpdateCourse,
                            }
                        ]
                    },
                    {
                        path: '/ManageProject',
                        name: 'จัดการข้อมูลโครงการ',
                        component: ManageProject,
                        children: [
                            {
                                path: 'planProject',
                                name: 'แผนโครงการ',
                                component: planProject,
                            },
                            {
                                path: 'Project',
                                name: 'ข้อมูลโครงการ',
                                component: ProjectData,
                            },
                            {
                                path: 'InsertProject',
                                name: 'เพิ่มข้อมูลโครงการ',
                                component: InsertProject,
                            },
                            {
                                path: 'UpdateProject',
                                name: 'แก้ไขข้อมูลโครงการ',
                                component: UpdateProject,
                            },
                            {
                                path: 'SettingData',
                                name: 'ตั้งค่าข้อมูล',
                                component: SettingData,
                            }
                        ]
                    },
                    {
                        path: '/Propose_a_plan',
                        name: 'จัดการข้อมูลตั้งงบโครงการ',
                        component: Propose_a_plan,
                        children: [
                            {
                                path: 'PlanProject',
                                name: 'แผนตั้งงบโครงการ',
                                component: PlanProject,
                            },
                            {
                                path: 'InsertPlanProject',
                                name: 'เพิ่มข้อมูลแผนตั้งงบโครงการ',
                                component: InsertPlanProject,
                            },
                            {
                                path: 'EditPlanProject',
                                name: 'แก้ไขข้อมูลแผนตั้งงบโครงการ',
                                component: EditPlanProject,
                            }
                        ]
                    },
                    {
                        path: '/SettingDateForPlan',
                        name: 'SettingDateForPlan',
                        component: SettingDateForPlan
                    },
                    {
                        path: '/PositionPerCourse',
                        name: 'ตั้งค่าเงื่อนไขตำแหน่ง',
                        component: PositionPerCourse
                    },
                    {
                        path: '/SettingCompetency',
                        name: 'SettingCompetency',
                        component: SettingCompetency
                    },
                    {
                        path: '/SettingExpenses',
                        name: 'SettingExpenses',
                        component: SettingExpenses
                    },
                    {
                        path: '/SettingManagerial',
                        name: 'SettingManagerial',
                        component: SettingManagerial
                    },
                    {
                        path: '/SettingPermission',
                        name: 'SettingPermission',
                        component: SettingPermission
                    },
                    {
                        path: '/Report',
                        name: 'Report',
                        component: Report,
                        children: [
                            {
                                path: 'Report30per',
                                name: 'Report30per',
                                component: Report30per,
                            },
                            {
                                path: 'AcademicPosition',
                                name: 'AcademicPosition',
                                component: AcademicPosition,
                            }
                        ]
                    },
                    {
                        path: '/ManagePerson',
                        name: 'ManagePerson',
                        component: ManagePerson
                    },
                    {
                        path: '/Item_Code',
                        name: 'Item_Code',
                        component: Item_Code
                    },
                    {
                        path: '/PrintFormProject',
                        name: 'PrintFormProject',
                        component: PrintFormProject
                    },
                    {
                        path: '/Permission',
                        name: 'Permission',
                        component: Permission
                    },
                    {
                        path: '/Meeting',
                        name: 'Meeting',
                        component: Meeting,
                        children: [
                            {
                                path: 'MeetingData',
                                name: 'MeetingData',
                                component: MeetingData,
                            },
                            {
                                path: 'ManageMeeting',
                                name: 'ManageMeeting',
                                component: ManageMeeting,
                            }
                        ]
                    },
                    {
                        path: '/ReggisterMeeting',
                        name: 'ReggisterMeeting',
                        component: ReggisterMeeting
                    },
                    {
                        path: '/JoinMeetingPerson',
                        name: 'JoinMeetingPerson',
                        component: JoinMeetingPerson
                    },
                    {
                        path: '/AcademicFundsRemaining',
                        name: 'AcademicFundsRemaining',
                        component: AcademicFundsRemaining
                    },
                    {
                        path: '/Appointment',
                        name: 'Appointment',
                        component: Appointment,
                        children: [
                            {
                                path: 'ManageAppointment',
                                name: 'ManageAppointment',
                                component: ManageAppointment,
                            },
                            {
                                path: 'InsertAppointment',
                                name: 'InsertAppointment',
                                component: InsertAppointment,
                            }
                        ]
                    },
                ]
            },
            {
                path: '/ApproveProject/:ProjectCode',
                name: 'ApproveProject',
                component: ApproveProject,
                props: true
            },
            {
                path: '/ApproveEditProject/:ProjectCode',
                name: 'ApproveEditProject',
                component: ApproveEditProject,
                props: true
            },
            {
                path: '/ApproveTravelByDep/:ProjectCode',
                name: 'ApproveTravelByDep',
                component: ApproveTravelByDep,
                props: true
            }
        ]
})
