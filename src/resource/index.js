import Level_provider from './level_provider'
import position_provider from './positions_provider'
import Depart_provider from './depart_provider'
import Course_provider from './course_provider'
import serviceProvider from "@/resource/service_provider";
import staffProvider from "@/resource/staff_provider";
import projectProvider from "@/resource/project_provider";
import competencyProvider from "@/resource/competency_provider";
import permissionProvider from "@/resource/permission_provider";
import meetingProvider from "@/resource/meeting_provider";
import appointmentProvider from "@/resource/appointment_provider";

// Give arg to provider to start endpoint with specific path for example = xxx.com/api/person
export const LevelService = new Level_provider()
export const PositionService = new position_provider()
export const DepartService = new Depart_provider()
export const CourseService = new Course_provider()
export const ServiceProvider = new serviceProvider()
export const ServiceProject = new projectProvider()
export const ServiceStaff = new staffProvider()
export const CompetencyService = new competencyProvider()
export const PermissionService = new permissionProvider()
export const MeetingService = new meetingProvider()
export const AppointmentService = new appointmentProvider()
