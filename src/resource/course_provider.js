import HttpRequest from './http_request'

class courseProvider extends HttpRequest{
    initData(path) {
        return this.fetch(path)
    }
}

export default courseProvider