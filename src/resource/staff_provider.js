import HttpRequest from './http_request'

class staffProvider extends HttpRequest{
    initData(path) {
        return this.fetch(path)
    }
}

export default staffProvider