
import HttpRequest from './http_request'

class serviceProvider extends HttpRequest {
    exec (type, url, data) {
        return this.request(type, url, data)
    }
    initData(path) {
        return this.fetch(path)
    }
    editData(path, data, header){
        return this.update(path, data, header)
    }
    deleteData(path, data){
        return this.delete(path, data)
    }
    insertData(path, data){
        return this.insert(path, data)
    }
}

export default serviceProvider
