import HttpRequest from './http_request'

class permissionProvider extends HttpRequest{
    initData (path) {
        return this.fetch(path)
    }
    editData(path, data, header){
        return this.update(path, data, header)
    }
    deleteData(path, data){
        return this.delete(path, data)
    }
    insertData(path, data){
        return this.insert(path, data)
    }
}

export default permissionProvider
