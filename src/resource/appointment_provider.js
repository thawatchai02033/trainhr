import HttpRequest from './http_request'

class appointmentProvider extends HttpRequest{
    initData(path) {
        return this.fetch(path)
    }
}

export default appointmentProvider