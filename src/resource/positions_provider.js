import HttpRequest from './http_request'

class positionsProvider extends HttpRequest{
    initData (path) {
        return this.fetch(path)
    }
}

export default positionsProvider
