
import HttpRequest from './http_request'

class levelProvider extends HttpRequest {
    initData (path) {
        return this.fetch(path)
    }

}

export default levelProvider
