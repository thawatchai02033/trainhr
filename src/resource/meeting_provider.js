import HttpRequest from './http_request'

class meetingProvider extends HttpRequest{
    initData(path) {
        return this.fetch(path)
    }
}

export default meetingProvider